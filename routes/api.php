<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Blogs
 */
Route::get('blog/admin/all', 'BlogController@adminBlogs'); //radi
Route::get('blog/featured', 'BlogController@featured'); //radi
Route::post('blog/{blog}/upload', 'BlogController@blogFileUpload'); //
Route::post('blog/{blog}/update', 'BlogController@update'); //radi
Route::post('blog/{blog}/featured', 'BlogController@toggleFeatured'); //radi
Route::post('blog/{blog}/readlater', 'BlogController@toggleReadlater'); //
Route::post('blog/latest', 'BlogController@latest'); //radi
Route::resource('blog', 'BlogController', ['except' => [
    'create', 'edit', 'update'
]]); //
/**
 * Recipes
 */
Route::get('recipe/admin/all', 'RecipeController@adminRecipes'); //radi
Route::post('recipe/{recipe}/upload', 'RecipeController@recipeFileUpload'); //
Route::post('recipe/{recipe}/update', 'RecipeController@update'); //radi
Route::post('recipe/{recipe}/featured', 'RecipeController@toggleFeatured'); //radi
Route::post('recipe/{recipe}/readlater', 'RecipeController@toggleReadlater'); //
Route::post('recipe/latest', 'RecipeController@latest'); //radi
Route::resource('recipe', 'RecipeController', ['except' => [
    'index', 'create', 'edit', 'update'
]]); //
/**
 * Steps
 */
Route::post('step/{step}/upload', 'StepController@stepFileUpload'); //radi
Route::post('step/{step}/update', 'StepController@update'); //radi
Route::resource('step', 'StepController', ['except' => [
    'index', 'edit', 'update'
]]); //
/**
 * Tags
 */
Route::get('tag/admin/all', 'TagController@allTags');
Route::get('tag/top/{top?}', 'TagController@categories');
Route::get('tag/category/{tag}', 'TagController@category');
Route::post('tag/{tag}/update', 'TagController@update'); //radi
Route::resource('tag', 'TagController', ['except' => [
    'create', 'edit', 'update'
]]); //
/**
 * Wishes
 */
Route::get('wish/admin/all', 'WishController@adminIndex');
Route::post('wish/{wish}/update', 'WishController@update'); //radi
Route::post('wish/{wish}/resolve', 'WishController@resolveWish'); //radi
Route::resource('wish', 'WishController', ['except' => [
    'create', 'edit', 'update'
]]); //
/**
 * Slides
 */
Route::get('slide/admin/all', 'SlideController@adminIndex');
Route::post('slide/{slide}/upload', 'SlideController@slideFileUpload'); //radi
Route::post('slide/{slide}/update', 'SlideController@update'); //radi
Route::resource('slide', 'SlideController', ['except' => [
    'create', 'edit', 'update'
]]); //
/**
 * Users
 */
Route::post('user/register', 'UserController@register');
Route::post('user/authenticated', 'UserController@authenticated');
Route::post('user/authenticate', 'UserController@authenticate');
Route::post('user/subscribe', 'UserController@subscribe');
Route::post('user/{user}/update', 'UserController@update'); //radi
Route::post('user/{user}/upload', 'UserController@avatarFileUpload'); //radi
Route::resource('user', 'UserController', ['except' => [
    'create', 'edit', 'update'
]]); //

/**
 * Advertisement
 */
Route::get('advertisement', 'AdvertisementController@index')->name('advertisement.index');
Route::post('advertisement/{advertisement}', 'AdvertisementController@update')->name('advertisement.update');

Route::get('search/{keyword?}', 'SearchController@search');

Route::post('password/email', 'PasswordController@postEmail');
Route::post('password/reset', 'PasswordController@postReset');

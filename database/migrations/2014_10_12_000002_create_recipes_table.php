<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration {

    public function up() {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('thumbnail')->nullable()->default(null);
            $table->text('description');
            $table->text('ingredients');
            $table->text('equipment');
            $table->integer('user_id')->unsigned();
            $table->boolean('featured')->nullable()->default(false);
            $table->dateTime('featured_date')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('recipes', function (Blueprint $table) {
            $table->index('title');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::dropIfExists('recipes');
    }
}

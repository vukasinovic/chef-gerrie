<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->integer('user_id')->unsigned();
            $table->text('content');
            $table->string('image_path')->nullable()->default(null);
            $table->boolean('featured')->default(false);
            $table->dateTime('featured_date')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->index('title');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('blogs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\Constant;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('phonenumber', 50)->nullable();
            $table->string('email')->unique();
            $table->string('bio', 155)->nullable();
            $table->string('password');
            $table->string('title')->default('Member');
            $table->string('avatar')->default(Constant::USER_DEFAULT_AVATAR_PATH);
            $table->enum('role', array(Constant::ROLE_USER, Constant::ROLE_ADMIN, Constant::ROLE_STAFF))->default(Constant::ROLE_USER);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }
}

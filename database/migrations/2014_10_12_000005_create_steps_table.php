<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepsTable extends Migration {

    public function up() {
        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')->unsigned();
            $table->string('title', 255);
            $table->text('thumbnail')->nullable();
            $table->integer('step_number')->unsigned();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('steps', function (Blueprint $table) {
            $table->index('title');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::dropIfExists('steps');
    }
}

<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\Helpers\Constant;
use \App\Helpers\FileHandler;
use Carbon\Carbon;

class RecipeSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run() {
        $rand = rand(1, 56);
        $images = ['2.jpg', '5.jpg', '4.jpg', '6.png'];
        for ($i = 1; $i < 56; $i++) {
            $image = $images[rand(0, 3)];
            $extension = FileHandler::extension(Constant::absolutePath("RECIPE_IMAGES_PATH").$image);
            $thumbnail = md5(Carbon::now()."-".$i).".".$extension;
            FileHandler::copy(Constant::absolutePath("RECIPE_IMAGES_PATH").$image, Constant::absolutePath("RECIPE_IMAGES_PATH").$thumbnail);
            DB::table('recipes')->insert([
                'title' => "Recipes Title ".$i,
                'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in urna semper, dapibus leo aliquam, dignissim magna. In ac risus eget velit vulputate suscipit vitae et enim. Nam eget consectetur nunc. Vestibulum lacinia sit amet purus id pretium. Sed tristique vehicula turpis, congue venenatis orci. Etiam ultricies maximus enim a congue. Nam diam nulla, maximus vel ultrices eu, mollis quis risus. Vivamus vel orci non dolor congue hendrerit. Vivamus vitae elit mattis, elementum orci ac, hendrerit tellus. In in eleifend lectus, id commodo eros. Praesent elementum nibh in vestibulum mattis. Integer consectetur malesuada justo aliquam porttitor. In cursus, felis non accumsan sagittis, eros est faucibus neque, sit amet faucibus eros neque vitae lorem. Quisque eget feugiat massa. Cras quis tincidunt quam.",
                'created_at' => Carbon::now()->subMinutes(rand(0, 60)),
                'user_id' => rand(1, 2),
                'featured' => ($rand==$i)?true:false,
                'ingredients' => nl2br('15              Ladyfingers
1 C            Expresso Coffee, brewed
4 T             Sugar
1 lb 2 oz.   Mascarpone
2                Vanilla Beans
½ C           Cream Sherry
1                Orange, zest and juice reserved
4 oz            High quality Bittersweet Chocolate, shaved'),
                'equipment' => 'Deep dish about 8x10 inches, hand electric mixer,1 medium bowl, chef knife, zester, measuring spoons & cup.',
                'thumbnail' => Constant::RECIPE_IMAGES_PATH.$thumbnail,
                'featured_date' => ($rand==$i)?Carbon::now():null
            ]);
        }
    }
}

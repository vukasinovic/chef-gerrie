<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class BlogTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 55; $i++) {
            DB::table('blog_tag')->insert([
                'blog_id' => $faker->unique()->numberBetween(1, 55),
                'tag_id' => rand(1, 5)
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use \App\Tag;

class TagSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $cat = new Tag;
    $cat->title = "Beans";
    $cat->save();

    $cat = new Tag;
    $cat->title = "Green Saladeans";
    $cat->save();

    $cat = new Tag;
    $cat->title = "Peas";
    $cat->save();

    $cat = new Tag;
    $cat->title = "Salad";
    $cat->save();

    $cat = new Tag;
    $cat->title = "Potatoes";
    $cat->save();
  }
}

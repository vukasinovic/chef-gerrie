<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use App\Helpers\Constant;
use App\Helpers\FileHandler;
use Carbon\Carbon;

class BlogSeeder extends Seeder {
    /**
    * Run the database seeds.re
    *
    * @return void
    */
    public function run() {
        $rand = rand(1, 56);
        $images = ['2.jpg', '5.jpg', '4.jpg', '6.png'];
        for ($i = 1; $i < 56; $i++) {
            $image = $images[rand(0, 3)];
            $extension = FileHandler::extension(Constant::absolutePath("BLOG_IMAGES_PATH").$image);
            $image_path = md5(Carbon::now()."-".$i).".".$extension;
            FileHandler::copy(Constant::absolutePath("BLOG_IMAGES_PATH").$image, Constant::absolutePath("BLOG_IMAGES_PATH").$image_path);
            DB::table('blogs')->insert([
                'title' => "Blog title ".$i,
                'content' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in urna semper, dapibus leo aliquam, dignissim magna. In ac risus eget velit vulputate suscipit vitae et enim. Nam eget consectetur nunc. Vestibulum lacinia sit amet purus id pretium. Sed tristique vehicula turpis, congue venenatis orci. Etiam ultricies maximus enim a congue. Nam diam nulla, maximus vel ultrices eu, mollis quis risus. Vivamus vel orci non dolor congue hendrerit. Vivamus vitae elit mattis, elementum orci ac, hendrerit tellus. In in eleifend lectus, id commodo eros. Praesent elementum nibh in vestibulum mattis. Integer consectetur malesuada justo aliquam porttitor. In cursus, felis non accumsan sagittis, eros est faucibus neque, sit amet faucibus eros neque vitae lorem. Quisque eget feugiat massa. Cras quis tincidunt quam. Pellentesque pulvinar erat et ex viverra, ac cursus sapien finibus. Pellentesque semper risus mattis purus pulvinar, quis auctor elit feugiat. Donec elit orci, venenatis sit amet eleifend ut, auctor nec arcu. Proin ultrices vestibulum est, sed porta lectus imperdiet vel. Etiam porta tempus leo vitae congue. Nulla a convallis urna. Cras porttitor sapien velit, sit amet ultricies orci feugiat sed. Sed posuere viverra libero at laoreet. Etiam auctor lorem vitae nulla vulputate, eu aliquet neque commodo. Nullam vel ornare velit. In a dolor urna. Proin maximus mollis risus, nec elementum elit vestibulum eu. Sed quam magna, faucibus quis ornare vitae, maximus fringilla dolor. Mauris euismod leo dui, vitae consectetur velit egestas at. Proin imperdiet, magna a volutpat dictum, mi dui sodales nunc, sit amet ullamcorper mi ipsum vitae mi. Aenean blandit tellus et arcu pulvinar, ac dictum ligula eleifend. Donec sed lectus in libero facilisis sodales. Integer eleifend sem sit amet eleifend fermentum. Ut posuere, orci sed cursus tempor, dolor turpis facilisis sem, sit amet ornare ipsum enim eu dui. Pellentesque rhoncus consectetur magna, quis malesuada nunc vulputate id. Proin in velit sollicitudin, rutrum magna eget, tincidunt enim. Pellentesque aliquam aliquam nunc non porttitor. Nunc viverra molestie nibh, et ullamcorper sem consequat eu. Donec volutpat tristique metus, quis dictum ligula tempus vitae. Sed commodo imperdiet diam, nec tincidunt mauris facilisis non. Morbi felis nisi, scelerisque vitae velit non, molestie dictum diam. Cras purus nibh, fringilla sed vestibulum eget, fermentum non purus. Cras ac turpis id felis volutpat porttitor. Curabitur a elit velit. Vivamus scelerisque lectus a laoreet iaculis. Nam convallis massa at mollis ultricies. Pellentesque mollis posuere iaculis. Nam vel iaculis ligula. Fusce dolor sem, convallis vitae sem in, lacinia molestie purus. Etiam quam justo, faucibus eget enim nec, tincidunt fermentum sapien. Aliquam ultrices, dui ut lacinia commodo, nunc purus feugiat est, ut condimentum lectus risus efficitur nisl. Mauris bibendum lectus in blandit placerat. Nullam fermentum nisi tortor, id elementum dui fermentum in. Ut elit nisi, fringilla convallis enim ut, elementum fermentum metus. Nunc efficitur risus non gravida consectetur. Cras sagittis sodales nulla, vitae venenatis metus hendrerit efficitur. Ut gravida bibendum arcu at ultricies. Donec massa urna, consectetur vel lacus porta, tempor hendrerit mauris.",
                'created_at' => Carbon::now()->subMinutes(rand(0, 60)),
                'user_id' => rand(1, 2),
                'image_path' => Constant::BLOG_IMAGES_PATH.$image_path,
                'featured' => ($rand==$i)?true:false,
                'featured_date' => ($rand==$i)?Carbon::now():null
            ]);
        }

    }
}

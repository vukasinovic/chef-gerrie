<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->call(UserSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(RecipeSeeder::class);
        $this->call(SlidesSeeder::class);
        $this->call(WishSeeder::class);
        $this->call(StepSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(UserWishSeeder::class);
        $this->call(BlogTagSeeder::class);
        $this->call(AdvertisementSeeder::class);
    }
}

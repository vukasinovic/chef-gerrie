<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserWishSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i = 1; $i < 4; $i++) {
            DB::table('user_wish')->insert([
                'wish_id' => $i,
                'user_id' => 1
            ]);
        }
    }
}

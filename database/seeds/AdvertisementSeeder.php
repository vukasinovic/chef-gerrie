<?php

use Illuminate\Database\Seeder;
use App\Advertisement;

class AdvertisementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ad = new Advertisement();
        $ad->save();
    }
}

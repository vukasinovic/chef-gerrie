<?php

use Illuminate\Database\Seeder;
use App\Slide;

class SlidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $slide = new Slide();
         $slide->image_path = "images/slides/alan_wake.png";
         $slide->save();

         $slide = new Slide();
         $slide->image_path = "images/slides/dmc.png";
         $slide->save();

         $slide = new Slide();
         $slide->image_path = "images/slides/smoke_grenade.jpg";
         $slide->save();

         $slide = new Slide();
         $slide->image_path = "images/slides/slide_1.png";
         $slide->activated = false;
         $slide->save();
    }
}

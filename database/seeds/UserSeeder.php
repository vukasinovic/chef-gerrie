<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Helpers\Constant;

class UserSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

      $user = new User;
      $user->firstname = 'John';
      $user->lastname = 'Deer';
      $user->phonenumber = '+381601234567';
      $user->email = 'admin@demo.com';
      $user->password = '123';
      $user->bio = 'Lorem ipsum dolor sit amet, lorem ipsum dolor sit amet.';
      $user->role = Constant::ROLE_ADMIN;
      $user->title = "Professional Chef";
      $user->save();

      $user = new User;
      $user->firstname = 'Jack';
      $user->lastname = 'Whealer';
      $user->phonenumber = '+381601234567';
      $user->email = 'jack.whealer@demo.com';
      $user->password = '123';
      $user->role = Constant::ROLE_STAFF;
      $user->title = "Staff Member";
      $user->save();

      $user = new User;
      $user->firstname = 'Rodney';
      $user->lastname = 'Jackson';
      $user->phonenumber = '+381601234567';
      $user->email = 'rodney.jackson@demo.com';
      $user->password = '123';
      $user->role = Constant::ROLE_USER;
      $user->title = "Member";
      $user->save();

  }
}

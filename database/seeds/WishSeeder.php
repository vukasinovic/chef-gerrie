<?php

use App\Wish;
use Illuminate\Database\Seeder;

class WishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wish_list = new Wish;
        $wish_list->user_id = 1;
        $wish_list->title = "WishList 1 Title";
        $wish_list->content = "WishList 1 Content";
        $wish_list->save();

        $wish_list = new Wish;
        $wish_list->user_id = 2;
        $wish_list->title = "WishList 2 Title";
        $wish_list->content = "WishList 2 Content";
        $wish_list->save();

        $wish_list = new Wish;
        $wish_list->user_id = 3;
        $wish_list->title = "WishList 3 Title";
        $wish_list->content = "WishList 3 Content";
        $wish_list->save();

        $wish_list = new Wish;
        $wish_list->user_id = 3;
        $wish_list->title = "WishList 4 Title";
        $wish_list->content = "WishList 4 Content";
        $wish_list->active = false;
        $wish_list->save();
    }
}

<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chef Jungle</title>
    <script src="js/script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="bower_components/angular-ui-notification/dist/angular-ui-notification.min.css">
    <link rel="stylesheet" type="text/css" href="bower_components/angular-chips/dist/main.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/md-chips-select/md-chips-select.css">
    <link rel="stylesheet" href="css/animated/animated.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-controller="BladeController" ng-cloak>
    <div bs-loading-overlay>
        <div id="wrapper">
            <div class="admin-menu">
                <div id="sidebar-wrapper">
                    <div class="sidebar-header">
                        <div class="row">
                            <div class="col-xs-3 col-xs-offset-1"><img src="img/admin-logo.png" alt="" class="img-responsive admin-side-logo"></div>
                            <div class="col-xs-5"><span class="title">Chef <span class="black-font">Jungle</span></span></div>
                            <div class="col-xs-3"><a class="arrow-menu-btn menu-toggle" style="font-size:30px;cursor:pointer"><i class="fa fa-angle-right" aria-hidden="true"></i><i class="fa fa-angle-left hide-arrow" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <ul class="sidebar-nav sidebar-content">
                        <!-- <li>
                        <h5>DASHBOARD</h5>
                    </li>
                    <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin" class="menu-toggle"><i class="fa fa-area-chart sidemenu-icon" aria-hidden="true"></i> Admin dashboard</a></div></div>
                </li> -->
                <li>
                    <h5>USER MANAGMENT</h5>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/users" class="menu-toggle"><i class="fa fa-user-o sidemenu-icon" aria-hidden="true"></i> Users</a></div></div>
                </li>
                <li>
                    <h5>DATA ADMINISTRATIONS</h5>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/blog" class="menu-toggle"><i class="fa fa-pencil-square-o sidemenu-icon" aria-hidden="true"></i> Blog</a></div></div>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/recipes" class="menu-toggle"><i class="fa fa-file-text-o sidemenu-icon" aria-hidden="true"></i> Recipes</a></div></div>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/tags-categories" class="menu-toggle"><i class="fa fa-tag sidemenu-icon" aria-hidden="true"></i> Tags/Categories</a></div></div>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/slides" class="menu-toggle"><i class="fa fa-picture-o sidemenu-icon" aria-hidden="true"></i> Slides</a></div></div>
                </li>
                <li>
                    <h5>WISH LIST MANAGMENT</h5>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/wish-list" class="menu-toggle"><i class="fa fa-list-alt sidemenu-icon" aria-hidden="true"></i> Wish</a></div></div>
                </li>
                <li>
                    <h5>ADVERTISEMENT MANAGMENT</h5>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="#!/admin/advertisement" class="menu-toggle"><i class="fa fa-buysellads sidemenu-icon" aria-hidden="true"></i> Advertisement</a></div></div>
                </li>
                <li>
                    <h5>NEWSLETTER SUBSCRIPTION</h5>
                </li>
                <li>
                    <div class="row"><div class="col-md-11 col-md-offset-1"><a href="https://us14.admin.mailchimp.com/" target="_blank" class="menu-toggle"><i class="fa fa-envelope-o sidemenu-icon" aria-hidden="true"></i> Newsletters</a></div></div>
                </li>
            </ul>
        </div>
        <nav id="admin-nav-bar" class="navbar navbar-default">
            <div class="navbar-header left-arrow-margin">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="arrow-menu-btn" style="font-size:30px;cursor:pointer" id="menu-toggle" ><i class="fa fa-angle-right" aria-hidden="true"></i><i class="fa fa-angle-left hide-arrow" aria-hidden="true"></i></a>
            </div>
            <div class="collapse navbar-collapse" id="admin-navbar-collapse">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 hidden-xs">
                            <div class="col-md-5 col-sm-5 navbar-btn admin-menu-item-margin">
                                <a ng-href="@{{ '#!/admin/users' }}"><img src="img/logo_footer.png" class="img-responsive" /></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 hidden-xs">
                            <div class="col-md-7 col-sm-12 col-xs-12 left-arrow-margin pull-right">
                                <div class="col-md-3 col-sm-3 col-lg-3 header-img"><img ng-src="@{{ getAuthenticatedUserData().avatarPath }}" height="43" alt="..." class="img-circle"></div>
                                <div class="col-md-6 col-sm-6 col-lg-7">
                                    <span class="user-name">@{{ getAuthenticatedUserData().fullName }}</span>
                                    <br />
                                    <span class="user-role">@{{ getAuthenticatedUserData().title }}</span>
                                </div>
                                <div class="col-md-3 col-sm-3 col-lg-2">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle admin-settings-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a ng-click="goHome();"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                                            <li><a ng-click="logout();"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 visible-xs text-center mobile-menu-padding">
                            <div class="col-xs-6 menu-item">
                                <a class="btn btn-primary" ng-click="goHome();"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                            </div>
                            <div class="col-xs-6 menu-item">
                                <a class="btn btn-danger" ng-click="logout();"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <nav id="main-nav-bar" class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="visible-xs-block">
                    <div class="col-xs-12">
                        <a href="#!/" class="visible-xs-inline-block navbar-btn">
                            <img src="img/logo_header.png" class="img-responsive">
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row hidden-lg hidden-sm hidden-md">
                    <div class="col-xs-12">
                        <div class="inner-addon left-addon">
                            <i class="fa fa-search search-fa" aria-hidden="true"></i>
                            <input type="text" class="search-input input-search" placeholder="Search for..." ng-model="searchInput.keyword" ng-model-options="{debounce: 300}" my-enter="search(searchInput.keyword);">
                            <i ng-if="searchInput.keyword && searchInput.keyword.length>0" ng-click="clearSearch()" class="fa fa-close clear-fa" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 menu-item menu-link text-center navbar-btn">
                        <a ng-click="goAbout();">About</a>
                    </div>
                    <div class="col-xs-12 menu-item menu-link text-center navbar-btn">
                        <a ng-click="goWishList();">Wish list</a>
                    </div>
                    <div class="col-xs-12 menu-item menu-link text-center navbar-btn">
                        <a ng-click="goReadLater();">Read Later</a>
                    </div>
                    <div class="col-xs-12 menu-item">
                        <div class="row">
                            <div class="col-xs-6 clearfix">
                                <button type="button" ng-click="goExplore()" class="btn btn-explore navbar-btn pull-right">EXPLORE</button>
                            </div>
                            <div class="col-xs-6" ng-if="!isLoggedIn">
                                <button type="button" class="btn btn-login navbar-btn" ng-click="goLogin()">LOGIN</button>
                            </div>
                            <div class="col-xs-6" ng-if="isLoggedIn && isUser">
                                <button type="button" class="btn btn-login navbar-btn" ng-click="logout()"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                    LOGOUT</button>
                                </div>
                                <div class="col-xs-6" ng-if="isLoggedIn && isAdmin">
                                    <button type="button" class="btn btn-login navbar-btn" ng-click="goAdminPanel()"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                        ADMIN</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden-xs">
                            <div class="col-md-4 col-sm-12 text-center left-arrow-margin">
                                <div class="col-md-6 col-sm-6 navbar-btn visible-sm-inline-block visible-md-inline-block visible-lg-inline-block">
                                    <a href="#!/">
                                        <img src="img/logo_header.png" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-6 navbar-btn">
                                    <div class="inner-addon left-addon">
                                        <i class="fa fa-search search-fa" aria-hidden="true"></i>
                                        <input type="text" class="search-input input-search" placeholder="Search for..."  ng-model="searchInput.keyword" ng-model-options="{debounce: 300}" my-enter="search(searchInput.keyword);">
                                        <i ng-if="searchInput.keyword && searchInput.keyword.length>0" ng-click="clearSearch()" class="fa fa-close clear-fa" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-md-offset-1 col-sm-12 text-center navbar-btn">
                                <div class="col-md-2 col-sm-2 menu-item menu-link text-center navbar-btn" ng-class="getClass('/about')">
                                    <a ng-click="goAbout();">About</a>
                                </div>
                                <div class="col-md-2 col-sm-2 menu-item menu-link text-center navbar-btn" ng-class="getClass('/wish-list')">
                                    <a ng-click="goWishList();">Wish list</a>
                                </div>
                                <div class="col-md-3 col-sm-3 menu-item menu-link text-center navbar-btn" ng-class="getClass('/read-later')">
                                    <a ng-click="goReadLater();">Read Later</a>
                                </div>
                                <div class="col-md-5 col-sm-5 menu-item">
                                    <div class="col-md-6 col-sm-6">
                                        <button type="button" ng-click="goExplore()" class="btn btn-explore navbar-btn">EXPLORE</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6" ng-if="!isLoggedIn">
                                        <button type="button" class="btn btn-login navbar-btn" ng-click="goLogin()">LOGIN</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6" ng-if="isLoggedIn && isUser">
                                        <button type="button" class="btn btn-login navbar-btn" ng-click="logout()"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                            LOGOUT</button>
                                        </div>
                                        <div class="col-md-6 col-sm-6" ng-if="isLoggedIn && isAdmin">
                                            <button type="button" class="btn btn-login navbar-btn" ng-click="goAdminPanel()"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                                ADMIN</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="carousel slide homeSlider animated fadeIn" ng-if="slides.length && loadedHome">
                        <ol class="carousel-indicators">
                            <li ng-repeat="slide in slides track by $index" data-target=".homeSlider" data-slide-to="@{{$index}}" ng-class="$index?0:'active'" contenteditable="false"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div ng-repeat="slide in slides track by $index" class="item" ng-class="$index?0:'active'">
                                <img ng-src="@{{slide.image_path}}" alt="" class="img-responsive" style="max-height: 400px; width: 100%;">
                            </div>
                        </div>
                        <a class="left carousel-control" href=".homeSlider" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href=".homeSlider" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <div ng-view autoscroll="true" class="mainContent"></div>
                </div>
                <div id="footer" ng-if="loadFooter">
                    <div class="container">
                        <div class="row footer-center">
                            <div class="col-md-6 footer-left">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/logo_footer.png" class="img-responsive center-block">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-2"><!--<a ng-click="goAbout();">About</a> --></div>
                                            <div class="col-md-3"><!--<a href="#!/">Advertisers</a> --></div>
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4 social-footer-icons">
                                                <div class="row">
                                                    <div class="col-xs-4 social">
                                                        <a class="btn btn-social social-btn-footer">
                                                            <span class="fa fa-facebook"></span>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-4 social">
                                                        <a class="btn btn-social social-btn-footer">
                                                            <span class="fa fa-instagram"></span>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-4 social">
                                                        <a class="btn btn-social social-btn-footer">
                                                            <span class="fa fa-twitter"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 footer-right">
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3 text-center"><a ng-hide="isLoggedIn" ng-click="goLogin();">Login</a></div>
                                            <div class="col-md-3 text-center"><a ng-click="goExplore();">Explore</a></div>
                                            <div class="col-md-3"><a ng-click="goHome();">Home</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-10">
                                                <p>All rights reserved. Chef Jungle Inc  © 2016.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts -->
            <script src="bower_components/jquery/dist/jquery.min.js"></script>
            <script src="bower_components/tinymce/tinymce.js"></script>
            <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="bower_components/angular/angular.js"></script>
            <script src="bower_components/angular-recaptcha/release/angular-recaptcha.min.js"></script>
            <script src="bower_components/angular-ui-notification/dist/angular-ui-notification.min.js"></script>
            <script src="bower_components/angular-chips/dist/angular-chips.min.js"></script>
            <script src="bower_components/angular-animate/angular-animate.js"></script>
            <script src="bower_components/md-chips-select/md-chips-select.js"></script>
            <script src="bower_components/angular-loading-overlay-spinjs/dist/angular-loading-overlay-spinjs.js"></script>
            <script src="bower_components/angular-loading-overlay/dist/angular-loading-overlay.js"></script>
            <script src="bower_components/angular-disqus/angular-disqus.min.js"></script>
            <script src="bower_components/angular-disqus-api/src/disqusApi.js"></script>
            <script src="bower_components/angular-socialshare/dist/angular-socialshare.min.js"></script>
            <script src="bower_components/angular-ui-tinymce/src/tinymce.js"></script>
            <script src="bower_components/angular-file-upload/dist/angular-file-upload.min.js"></script>
            <script src="bower_components/angular-route/angular-route.min.js"></script>
            <script src="bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
            <script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
            <script src="bower_components/angular-css/angular-css.min.js"></script>
            <script src="libs/ui-bootstrap-tpls-2.4.0.min.js"></script>
            <script src="js/app.js"></script>
            <script
            src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"
            async defer
            ></script>
            <script src="js/controllers.js"></script>
            <script src="js/services.js"></script>
            <script>
            $("#wrapper").toggleClass("toggled");
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
                $(".fa-angle-right").toggleClass("hide-arrow");
                $(".fa-angle-left").toggleClass("hide-arrow");
            });

            $(".menu-toggle").click(function(e) {
                $("#wrapper").toggleClass("toggled");
                $(".fa-angle-right").toggleClass("hide-arrow");
                $(".fa-angle-left").toggleClass("hide-arrow");
            });
            </script>
        </body>
        </html>

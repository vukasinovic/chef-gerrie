@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container content">
    <div class="panel panel-default login-panel center-block">
        <div class="panel-body">
            <h2 class="text-center login-title">Reset password</h2>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                            {{ csrf_field() }}
                           <div class="row">
                               <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0">
                                <input placeholder="Email address" id="email" type="email" class="login_box" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                               </div>
                           </div>
                            <div class="row buttons-login">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn signin-button">
                                        Send Link
                                    </button>
                                </div>
                                <!-- <div class="col-xs-6 col-sm-4 col-sm-offset-0 col-md-6 col-md-offset-0">
                                </div> -->
                            </div>

                            <!--<div class="row">-->
                                <!--<div class="col-xs-12">-->
                                    <!--<p class="text-center or-login-text">Or login with your social network</p>-->
                                <!--</div>-->
                            <!--</div>-->
                            <!--<div class="row social-buttons center-block">-->
                                <!--<div class="col-xs-4"><button class="btn fb-btn"><i class="fa fa-facebook" aria-hidden="true"></i></button></div>-->
                                <!--<div class="col-xs-4"><button class="btn tw-btn"><i class="fa fa-twitter" aria-hidden="true"></i></button></div>-->
                                <!--<div class="col-xs-4"><button class="btn gp-btn"><i class="fa fa-google-plus" aria-hidden="true"></i></button></div>-->
                            <!--</div>-->
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div><!-- /container -->

@endsection


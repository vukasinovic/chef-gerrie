@extends('layouts.app')

@section('content')
<div class="container content">
    <div class="panel panel-default login-panel center-block">
        <div class="panel-body">
            <h2 class="text-center login-title">Reset password</h2>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                         <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                             <input type="hidden" name="token" value="{{ $token }}">
                            {{ csrf_field() }}
                           <div class="row">
                               <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0">
                                <input placeholder="E-mail address" id="email" type="email" class="login_box" name="email" value="{{ $email or old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0">
                                    <input placeholder="Password" id="password" type="password" class="login_box" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0">
                                    <input placeholder="Confirm password" id="password-confirm" type="password" class="login_box" name="password_confirmation" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                               </div>
                           </div>
                            <div class="row buttons-login">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn signin-button">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div><!-- /container -->
@endsection
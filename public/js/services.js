var myAppServices = angular.module('myAppServices', [
    'LocalStorageModule'
]);
myAppServices.factory('UserService', ['$http', 'localStorageService', '$location', function($http, localStorageService, $location) {
    function checkIfLoggedIn() {
        if(localStorageService.get('token'))
            return true;
        else
            return false;
    }
    function signup(name, email, password, onSuccess, onError) {
        $http.post('/api/auth/signup',
            {
                name: name,
                email: email,
                password: password
            }).
        then(function(response) {
            localStorageService.set('token', response.data.token);
            onSuccess(response);
        }, function(response) {
            onError(response);
        });
    }
    function login(email, password, onSuccess, onError){
        $http.post('/api/user/authenticate',
            {
                email: email,
                password: password
            }).
        then(function(response) {
            localStorageService.set('token', response.data.token);
            onSuccess(response);
        }, function(response) {
            onError(response);
        });
    }
    function logout() {
        localStorageService.remove('userId');
        localStorageService.remove('token');
        localStorageService.remove('fullName');
        localStorageService.remove('role');
        localStorageService.remove('title');
        localStorageService.remove('avatarPath');
        $location.path('/login');
    }
    function getAuthenticatedUserData() {
        var obj = {
            'avatarPath': localStorageService.get('avatarPath'),
            'token': localStorageService.get('token'),
            'role': localStorageService.get('role'),
            'title': localStorageService.get('title'),
            'userId': localStorageService.get('userId'),
            'fullName': localStorageService.get('fullName')
        };
        return obj;
    }
    function setAuthenticatedUserData(attributes) {
        localStorageService.set('avatarPath', attributes.avatar);
        localStorageService.set('fullName', attributes.firstname+' '+attributes.lastname);
        localStorageService.set('role', attributes.role);
        localStorageService.set('title', attributes.title);
    }
    function getAvatarPath() {
        return localStorageService.get('avatarPath');
    }
    function setAvatarPath(avatarPath) {
        localStorageService.set('avatarPath', avatarPath);
    }
    function getFullName() {
        return localStorageService.get('fullName');
    }
    function setFullName(fullName) {
        localStorageService.set('fullName', fullName);
    }
    function getCurrentToken() {
        return localStorageService.get('token');
    }
    function setCurrentToken(token) {
        localStorageService.set('token', token);
    }
    function getRole(){
        return localStorageService.get('role');
    }
    function setRole(role) {
        localStorageService.set('role', role);
    }
    function getTitle() {
        return localStorageService.get('title');
    }
    function setTitle(title) {
        localStorageService.set('title', title);
    }
    function getUserId() {
        return localStorageService.get('userId');
    }
    return {
        checkIfLoggedIn: checkIfLoggedIn,
        signup: signup,
        login: login,
        logout: logout,
        getAvatarPath: getAvatarPath,
        setAvatarPath: setAvatarPath,
        getTitle: getTitle,
        setTitle: setTitle,
        getRole: getRole,
        setRole: setRole,
        getCurrentToken: getCurrentToken,
        setCurrentToken: setCurrentToken,
        getFullName: getFullName,
        setFullName: setFullName,
        getAuthenticatedUserData: getAuthenticatedUserData,
        setAuthenticatedUserData: setAuthenticatedUserData,
        getUserId: getUserId
    }
}]);
myAppServices.service('HttpGetServices', ['$http', function($http) {
    function getListData(url) {
        return $http.get(url)
            .then(function(response) {
                return response.data;
            })
    }
    function getSingleData(url, id) {
        var urlSingle = url + '/' + id;
        return $http.get(urlSingle)
            .then(function(response) {
                return response.data;
            })
    }
    function getWithToken(url, token) {
        return $http.get(url,{
            headers: {'Authorization': 'Bearer ' + token}
            })
            .then(function(response) {
                return response.data;
            })
    }
    return {
        endpointBlogList: '/api/blog',
        endpointAdminBlogList: '/api/blog/admin/all',
        endpointGetUser: '/api/user',
        endpointListUsers: '/api/user',
        endpointBlogsWithCat: '/api/tag/category/',
        endpointHomeList: '/api/home/all',
        endpointRecipesList: '/api/recipe/admin/all',
        endpointWishList: '/api/wish/admin/all',
        endpointWishListWithUser: '/api/wish/',
        endpointBlogCategories: '/api/tag/top/5',
        endpointReadLater: '/api/readlater/get',
        endpointGetFeatureBlog: '/api/blog/featured',
        endpointGetFeatureRecipes: '/api/recipes/feature-content/get',
        endpointGetSingleRecipe: '/api/recipe',
        endpointGetDashboardData: '/api/dashboard/data',
        endpointLatestTags: '/api/tag',
        endpointAllTags: '/api/tag/admin/all',
        endpointAdvertisement: '/api/advertisement',
        getListData: getListData,
        getSingleData: getSingleData,
        getWithToken: getWithToken
    }
}]);
myAppServices.service('HttpPostServices', ['$http', function($http) {
    function postData(endpoint, params, token, onSuccess) {
        $http.post(endpoint, params,
            {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }
        ).
        then(function(response) {
            onSuccess(response);
        });
    }
    return {
        disqusGetUserToken: 'https://disqus.com/api/oauth/2.0/access_token',
        registerUser: '/api/user/register',
        resetPassword: '/api/password/email',
        addUser: 'api/user/',
        editUser: 'api/user/',
        endpointAuthenticatedUser: '/api/user/authenticated',
        endpointAddTag: '/api/tag',
        endpointUpdateRole: '/api/users/change-permission',
        endpointRegisterUser: '/api/disqus-login',
        endpointAddBlog: '/api/blog/',
        endpointSearch: '/api/search/',
        endpointAddWishList: '/api/wish/',
        endpointLikeWishlist: '/api/wishlist/',
        endpointRecipeFeature: '/api/recipe/', //pass id /featured
        endpointBlogFeature: '/api/blog/', //pass id /featured
        endpointAddBlogCategory: '/api/blog/category/add',
        endpointAddRecipe: '/api/recipe/',
        endpointUpdateRecipe: '/api/recipe/',
        endpointUpdateBlog: '/api/blog/post/', //pass id of blog /update
        endpointAddRecipeSteps: '/api/step/', //pass id then /add
        endpointCreateBlogReadLater: '/api/blog/', //pass id of blog /readlater
        endpointSubscribe: '/api/user/subscribe',
        endpointLatestBlogs: '/api/blog/latest',
        endpointLatestRecipes: '/api/recipe/latest',
        endpointResolveWish: '/api/wish/',
        postData: postData
    }
}]);
myAppServices.service('HttpDeleteServices', ['$http', function($http) {
    function deleteData(endpoint, token, onSuccess) {
        $http.delete(endpoint, {
                headers: {'Authorization': 'Bearer ' + token}
            }
        ).
        then(function(response) {
            onSuccess(response);
        });
    }
    return {
        endpointDeleteBlogCategory: '/api/blog/category/',
        endpointDeleteBlog: '/api/blog/',
        endpointDeleteRecepie: '/api/recipe/',
        deleteData: deleteData
    }
}]);
myAppServices.service('HttpUpdateServices', ['$http', function($http) {
    function updateData(endpoint, params, token, onSuccess) {
        $http.post(endpoint, params,
            {
                headers: {'Authorization': 'Bearer ' + token}
            }
        ).
        then(function(response) {
            onSuccess(response);
        });
    }
    return {
        endpointUpdateBlogCategory: '/api/blog/category/',
        endpointUpdateBlog: '/api/blog/', //pass id of blog
        endpointUpdateRecepie: 'api/recipe/', // pass id /update
        endpointUpdateWishlist: '/api/wishlist/', //pass id /update
        endpointUpdateSlide: '/api/slide/',
        endpointAdvertisement: '/api/advertisement',
        updateData: updateData
    }
}]);
myAppServices.service('parseHtmlService', ['$sce', '$filter', function($sce, $filter) {
    function parse(data) {
        return $sce.trustAsHtml(data);
    }
    return {
        parse: parse
    }
}]);
myAppServices.service('authInterceptor', function($q, $location, localStorageService, $injector) {
    var service = this;
    var notification = null;
    var getNotification = function() {
        if (!notification) {
            notification = $injector.get('Notification');
        }
        return notification;
    };
    service.responseError = function(response) {
        if(response.status == 500) {
            getNotification().error('Internal Server Error!');
        }
        if(response.status == 400) {
            //Add something if needed for this statusCode 400
            getNotification().info("Login first");
        }
        if (response.status == 401 || response.status == 403) {
            getNotification().error('Session Expired!');
            localStorageService.remove('token');
            localStorageService.remove('fullName');
            localStorageService.remove('role');
            localStorageService.remove('title');
            localStorageService.remove('avatarPath');
            $location.path('/login');
        }
        if(response.status == 422) {
            angular.forEach(response.data, function(value) {
                angular.forEach(value, function(message) {
                    getNotification().error({message: message});
                });
            });
        }
        return $q.reject(response);
    };
});

var myApp = angular.module('myApp', [
    'ngRoute',
    'ui.bootstrap',
    'vcRecaptcha',
    'ui-notification',
    'ngAnimate',
    'md.chips.select',
    'angular.chips',
    'ngSanitize',
    'bsLoadingOverlay',
    'bsLoadingOverlaySpinJs',
    'ngDisqus',
    'ngDisqusApi',
    '720kb.socialshare',
    'ui.tinymce',
    'angularFileUpload',
    'myAppControllers',
    'myAppServices',
    'angularCSS'
]);
myApp.run(function($rootScope, $location, UserService, HttpPostServices, HttpGetServices, $timeout) {
    if($location.url() != '/') {
        $('.homeSlider').hide();
    } else {
        $('.homeSlider').show();
    }
    $rootScope.$on("$routeChangeStart", function(event, route) {
        $rootScope.isLoggedIn = UserService.checkIfLoggedIn();
        $rootScope.isAdmin = UserService.getRole() && UserService.getRole() == 'ROLE_ADMIN' || UserService.getRole() == 'ROLE_SUPER';
        $rootScope.isUser = UserService.getRole() && UserService.getRole() == 'ROLE_USER';
        if($location.url().substring(0,6) == '/admin' && UserService.getRole() == 'ROLE_USER') {
            $location.path('/');
        }
        if($location.url().substring(0,6) == '/admin') {
            $('.admin-menu').show();
            if(UserService.getCurrentToken() == null || UserService.getCurrentToken() == false || UserService.getCurrentToken() == '') {
                $location.path('/login');
                $('.admin-menu').hide();
            }
        } else {
            $('.admin-menu').hide();
        }
        if($location.url() != '/') {
            $('.homeSlider').hide();
        } else {
            $('.homeSlider').show();
        }
        if($location.url() == '/reset-password' || $location.url() == '/login' || $location.url() == '/register' || $location.url().substring(0,6) == '/admin') {
            $('.homeSlider, #footer, #main-nav-bar').hide();
        } else {
            $rootScope.loadFooter = false;
            if($location.url() == '/read-later') {
                $rootScope.loadFooter = true;
            }
            $timeout(function () {
                $rootScope.loadFooter = true;
            }, 500);
            $('#footer, #main-nav-bar').show();
        }
    });
});
myApp.config(['$routeProvider', 'NotificationProvider', '$locationProvider', '$disqusProvider', '$disqusApiProvider', 'vcRecaptchaServiceProvider', '$httpProvider', function($routeProvider, NotificationProvider, $locationProvider, $disqusProvider, $disqusApiProvider, vcRecaptchaServiceProvider, $httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
    vcRecaptchaServiceProvider.setSiteKey('6LfEuw4UAAAAAEl-wC7f6h7VVnU2GY5qX8xNiQxo');
    $disqusApiProvider.setApiKey('dOzSUC8bPF7WSljgFMC2x7NbRJpJVmh2LAhxesYul6DJBcPF1hsbAcFTLDUj9dMb');
    $locationProvider.hashPrefix('!');
    $disqusProvider.setShortname('chef-g');
    NotificationProvider.setOptions({
        delay: 2000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'left',
        positionY: 'bottom'
    });
    $routeProvider.
    when('/', {
        templateUrl: 'partials/home.html',
        controller: 'HomeController'
    }).
    when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController',
        css: 'css/login.css'
    }).
    when('/register', {
        templateUrl: 'partials/register.html',
        controller: 'RegisterController',
        css: 'css/register.css'
    }).
    when('/reset-password', {
        templateUrl: 'partials/reset-password.html',
        controller: 'ResetPasswordController',
        css: 'css/reset-password.css'
    }).
   /* when('/admin', {
        templateUrl: 'partials/admin/dashboard.html',
        controller: 'AdminDashboardController',
        css: ['css/admin-menu.css','css/admin.css']
    }).*/
     when('/admin/wish-list', {
     templateUrl: 'partials/admin/wish-list-admin.html',
     controller: 'WishListAdminController',
     css: ['css/wish-list-admin.css','css/admin-menu.css', 'css/admin.css']
     }).
    when('/admin/wish-list/:wishlistId', {
        templateUrl: 'partials/admin/wish-list-single.html',
        controller: 'WishListSingleAdminController',
        css: ['css/wish-list-single.css','css/admin-menu.css', 'css/admin.css']
    }).
    when('/explore', {
        templateUrl: 'partials/blog.html',
        controller: 'BlogController'
    }).
    when('/explore-view/:blogId', {
        templateUrl: 'partials/blog-view.html',
        controller: 'BlogViewController'
    }).
    when('/read-later', {
        templateUrl: 'partials/read-later.html',
        controller: 'ReadLaterController',
        css: 'css/read-later.css'
    }).
    when('/wish-list', {
        templateUrl: 'partials/wish-list.html',
        controller: 'WishListController',
        css: 'css/wish-list.css'
    }).
    when('/blogs-list/:categoryId/:categoryName', {
        templateUrl: 'partials/blogs-list.html',
        controller: 'BlogsListController',
        css: 'css/blogs-list.css'
    }).
    when('/recipe-single/:recipeId', {
        templateUrl: 'partials/recipe-single.html',
        controller: 'RecipeSingleController',
        css: 'css/recipe-single.css'
    }).
    when('/not-found-page', {
        templateUrl: 'partials/not-found-page.html',
        controller: 'NotFoundPageController',
        css: 'css/not-found-page.css'
    }).
    when('/thank-you', {
        templateUrl: 'partials/thank-you.html',
        controller: 'ThankYouController',
        css: 'css/thank-you.css'
    }).
    when('/search-results', {
        templateUrl: 'partials/search-results.html',
        controller: 'SearchResultsController',
        css: 'css/search-results.css'
    }).
    when('/admin/blog', {
        templateUrl: 'partials/admin/blog-admin.html',
        controller: 'BlogAdminController',
        css: ['css/admin-menu.css','css/blog-admin.css']
    }).
    when('/admin/users', {
        templateUrl: 'partials/admin/users.html',
        controller: 'UsersAdminController',
        css: ['css/admin-menu.css','css/users.css']
    }).
    when('/admin/recipes', {
        templateUrl: 'partials/admin/recipes-admin.html',
        controller: 'RecipesController',
        css: ['css/admin-menu.css','css/recipes.css']
    }).
    when('/admin/add-recipe', {
        templateUrl: 'partials/admin/add-recipe.html',
        controller: 'AddRecipeController',
        css: ['css/admin-menu.css','css/recipe-single.css']
    }).
    when('/admin/recipe/:recipeId', {
        templateUrl: 'partials/admin/recipe-single.html',
        controller: 'RecipeSingleController',
        css: ['css/admin-menu.css','css/recipe-single.css']
    }).
    when('/admin/edit-blog/:blogId', {
        templateUrl: 'partials/admin/edit-blog.html',
        controller: 'EditBlogController',
        css: ['css/admin-menu.css','css/blog-admin.css']
    }).
    when('/admin/add-blog', {
        templateUrl: 'partials/admin/add-blog.html',
        controller: 'AddBlogController',
        css: ['css/admin-menu.css','css/blog-admin.css']
    }).
    when('/admin/tags-categories', {
        templateUrl: 'partials/admin/tags-categories.html',
        controller: 'TagsCategoriesController',
        css: ['css/admin-menu.css','css/tags-categories.css', 'css/admin.css']
    }).
    when('/admin/add-admin', {
        templateUrl: 'partials/admin/add-admin.html',
        controller: 'AddAdminController',
        css: ['css/admin-menu.css','css/add-admin.css', 'css/admin.css']
    }).
    when('/admin/edit-admin/:userId', {
        templateUrl: 'partials/admin/edit-admin.html',
        controller: 'EditAdminController',
        css: ['css/admin-menu.css','css/edit-admin.css', 'css/admin.css']
    }).
    when('/admin/add-tags', {
        templateUrl: 'partials/admin/add-tags.html',
        controller: 'AddTagsController',
        css: ['css/admin-menu.css','css/add-tags.css', 'css/admin.css']
    }).
    when('/admin/slides', {
        templateUrl: 'partials/admin/slides.html',
        controller: 'SlidesController',
        css: ['css/admin-menu.css','css/slides.css', 'css/admin.css']
    }).
    when('/admin/edit-slides/:slideId', {
        templateUrl: 'partials/admin/edit-slides.html',
        controller: 'EditSlidesController',
        css: ['css/admin-menu.css','css/edit-slides.css', 'css/admin.css']
    }).
    when('/admin/add-slides', {
        templateUrl: 'partials/admin/add-slides.html',
        controller: 'AddSlidesController',
        css: ['css/admin-menu.css','css/add-slides.css', 'css/admin.css']
    }).
    when('/admin/advertisement', {
        templateUrl: 'partials/admin/advertisement.html',
        controller: 'AdvertisementController',
        css: ['css/admin-menu.css','css/advertisement.css', 'css/admin.css']
    }).
    when('/admin/edit-tags/:tagId', {
        templateUrl: 'partials/admin/edit-tags.html',
        controller: 'EditTagsController',
        css: ['css/admin-menu.css','css/edit-tags.css', 'css/admin.css']
    }).
    when('/admin/edit-recipe/:recipeId', {
        templateUrl: 'partials/admin/edit-recipe.html',
        controller: 'EditRecipeController',
        css: ['css/admin-menu.css','css/recipe-single.css']
    }).
    when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'AboutController',
        css: 'css/about.css'
    }).
    otherwise({
        redirectTo: '/'
    });

}]);
myApp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);
myApp.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});
myApp.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("change", function (event) {
            scope.$apply(function () {
                scope.$eval(attrs.myEnter);
            });
            event.preventDefault();
        });
    };
});
myApp.filter('limitHtml', function() {
    return function(text, limit) {
        var changedString = String(text).replace(/<[^>]+>/gm, '');
        var length = changedString.length;
        return changedString.length > limit ? changedString.substr(0, limit - 1) : changedString;
    }
});
myApp.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    }
    return {
        link: fn_link
    }
}]);
myApp.directive('formatPhone', [
    function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elem, attrs, ctrl, ngModel) {
                elem.add(phoneNumber).on('keyup', function() {
                    var origVal = elem.val().replace(/[^\w\s]/gi, '');
                    origVal = origVal.replace(/[^\d.-]/g,'');
                        jQuery("#phoneNumber").val('+' + origVal);
                });
            }
        };
    }
]);

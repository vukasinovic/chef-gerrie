var myAppControllers = angular.module('myAppControllers', []);
myAppControllers.controller('BladeController', ['$scope', '$location', 'UserService', 'Notification', 'HttpGetServices', 'parseHtmlService', 'localStorageService', 'HttpPostServices', '$rootScope', '$timeout', function ($scope, $location, UserService, Notification, HttpGetServices, parseHtmlService, localStorageService, HttpPostServices, $rootScope, $timeout) {
    $rootScope.searchInput = {
        keyword: null
    };
    $scope.loadedHome = false;
    $rootScope.searchResult = [];
    $rootScope.clearSearch = function () {
        $rootScope.searchInput.keyword = null;
    };
    $rootScope.$watch('searchInput.keyword', function (newValue, oldValue) {
        $rootScope.searchInput.keyword = newValue;
        if (newValue != oldValue) {
            if ($rootScope.searchInput.keyword != null) {
                $scope.blogTitle = true;
                $scope.recipeTitle = true;
                $rootScope.loadedSearch = false;
                HttpGetServices.getListData(HttpPostServices.endpointSearch + $rootScope.searchInput.keyword).then(function (response) {
                    $rootScope.loadedSearch = true;
                    if (response.length == 0) {
                        $location.path('/not-found-page');
                    } else if (response[0].length == 0 && response[1].length == 0) {
                        $location.path('/not-found-page');
                    } else {
                        $rootScope.searchResult = response;
                        console.log(response);
                        angular.forEach($rootScope.searchResult[0], function (blog, index) {
                            console.log(blog);
                            blog.created_at = Date.parse(blog.created_at);
                        });
                        angular.forEach($rootScope.searchResult[1], function (recipe, index) {
                            console.log(recipe);
                            recipe.created_at = Date.parse(recipe.created_at);
                        });
                        $location.path('/search-results');
                        $scope.$apply();
                    }
                });
            }
        }
    }, true);
    $scope.getAuthenticatedUserData = function () {
        return UserService.getAuthenticatedUserData();
    };
    $scope.getClass = function (path) {
        return ($location.path().substr(0, path.length) === path) ? 'menu-active' : '';
    };
    $scope.goAdminPanel = function () {
        $location.path('/admin/users');
    };
    $scope.goToDashboard = function () {
        $location.path('/admin/users');
    };
    $scope.gotToBlogs = function (category) {
        $location.path('/blogs-list/' + category.id + '/' + category.title);
    };
    $scope.goToSingleBlog = function (id) {
        $location.path('/explore-view/' + id);
    };
    $scope.goToSingleRecipe = function (id) {
        $location.path('/recipe-single/' + id);
    };
    HttpGetServices.getListData('api/slide').then(function (response) {
        $scope.slides = response.entity;
        $scope.loadedHome = true;
    });
    $('.carousel').carousel({
        interval: 4000
    });
    $scope.parseHtml = function (data) {
        return parseHtmlService.parse(data);
    };
    $scope.logout = function () {
        $scope.username = null;
        localStorageService.clearAll();
        Notification.success({message: 'Successfully logged out!'});
        UserService.logout();
    };
    $scope.goAbout = function () {
        $location.path('/about');
    };
    $scope.goWishList = function () {
        $location.path('/wish-list');
    };
    $scope.goReadLater = function () {
        $location.path('/read-later');
    };
    $scope.goLogin = function () {
        $location.path('/login');
    };
    $scope.goHome = function () {
        $location.path('/');
    };
    $scope.goExplore = function () {
        $location.path('/explore');
    };
}]);
myAppControllers.controller('LoginController', ['$http', '$scope', 'UserService', 'localStorageService', '$location', 'Notification', 'disqusApi', 'HttpPostServices', function ($http, $scope, UserService, localStorageService, $location, Notification, disqusApi, HttpPostServices) {
    $scope.register = function () {
        $location.path('/register');
    };
    $scope.user = {
        username: '',
        password: ''
    };
    if (UserService.checkIfLoggedIn() && UserService.getRole() == 'ROLE_ADMIN' || UserService.getRole() == 'ROLE_SUPER') {
        $location.path('/admin');
    } else if (UserService.checkIfLoggedIn() && UserService.getRole() == 'ROLE_USER') {
        $location.path('/');
    }
    $scope.login = function () {
        if ($scope.user.email == '' || $scope.user.password == '') {
            Notification.error({message: 'You must fill all fields!', delay: 1000});
        } else {
            UserService.login($scope.user.email, $scope.user.password, function (response) {
                var entity = response.data.entity;
                localStorageService.set('fullName', entity.firstname + " " + entity.lastname);
                localStorageService.set('title', entity.title);
                localStorageService.set('userId', entity.id);
                localStorageService.set('role', entity.role);
                localStorageService.set('token', entity.token);
                localStorageService.set('avatarPath', entity.avatar);
                Notification.success({message: 'Successfully logged in'});
                $scope.goToDashboard();
            }, function (error) {
                Notification.error({message: error.data.message, delay: 1000});
            });
        }
    }
}]);
myAppControllers.controller('HomeController', ['$scope', 'HttpPostServices', 'HttpGetServices', 'UserService', 'Notification', '$location', function ($scope, HttpPostServices, HttpGetServices, UserService, Notification, $location) {
    $scope.nextUrl = '';
    $scope.email = '';
    $scope.firstName = '';
    $scope.lastName = '';
    $scope.loadedHome = false;
    $scope.loadedMore = true;
    $scope.loadedFeatured = false;
    $scope.disableBtn = false;
    $scope.loadedCategories = false;
    $scope.page = 2;
    $scope.subscribe = function () {
        if ($scope.email == '' || $scope.firstName == '' || $scope.lastName == '') {
        } else {
            HttpPostServices.postData(HttpPostServices.endpointSubscribe, {
                email: $scope.email,
                fname: $scope.firstName,
                lname: $scope.lastName
            }, null, function (response) {
                $location.path('/thank-you');
            }, function (error) {
            });
        }
    };

    HttpGetServices.getWithToken(HttpGetServices.endpointBlogList + '?per_page=12', UserService.getCurrentToken()).then(function (response) {
        $scope.recipes = response;
        $scope.loadedHome = true;
    });
    HttpGetServices.getListData(HttpGetServices.endpointBlogCategories).then(function (response) {
        $scope.categories = response.entity;
        $scope.loadedCategories = true;
    });
    HttpGetServices.getListData('/api/blog/featured').then(function (data) {
        $scope.featured = data.entity;
        $scope.firstFeatured = data.entity[0];
        $scope.secondFeatured = data.entity[1];
        $scope.loadedFeatured = true;
    });
    $scope.loadMore = function () {
        $scope.loadedMore = false;
        var url = HttpGetServices.endpointBlogList + '?per_page=12&page=' + $scope.page;
        $scope.disableBtn = true;
        HttpGetServices.getListData(url).then(function (data) {
            angular.forEach(data, function (value) {
                var index = $scope.recipes.indexOf(value);
                if (index == -1) {
                    $scope.recipes.push(value);
                }
            });
            $scope.disableBtn = false;
            $scope.page += 1;
            $scope.loadedHome = true;
            $scope.loadedMore = true;
        });
    };

    $scope.addToReadLater = function (recipeId) {
        var readLaterData = {
            blog_id: null,
            recipes_id: recipeId
        };
        HttpPostServices.postData(HttpPostServices.endpointReadLater, readLaterData, UserService.getCurrentToken(), function (response) {
            Notification.success({message: 'Successfully added to "Read Later"'});
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('RecipeSingleController', ['$scope', 'Notification', '$routeParams', 'HttpGetServices', '$location', 'HttpPostServices', 'UserService', function ($scope, Notification, $routeParams, HttpGetServices, $location, HttpPostServices, UserService) {
    $scope.id = $routeParams.recipeId;
    $scope.loadedSingleRecipe = false;
    HttpGetServices.getWithToken(HttpGetServices.endpointGetSingleRecipe + '/' + $routeParams.recipeId, UserService.getCurrentToken()).then(function (response) {
        $scope.recipe = response.entity;
        $scope.recipe.created_at = Date.parse($scope.recipe.created_at);
        $scope.recipe.ingredients.replace('<br/>', '\r\n');
        $scope.disqusConfig = {
            disqus_shortname: 'chef-g',
            disqus_identifier: $scope.id + 'recipe',
            disqus_url: $location.absUrl()
        };
        $scope.loadedSingleRecipe = true;
    });
    HttpGetServices.getWithToken(HttpGetServices.endpointAdvertisement, UserService.getCurrentToken()).then(function(data) {
        $scope.ad = data.entity;
    });
    $scope.addToReadLater = function (recipe) {
        HttpPostServices.postData('api/recipe/' + recipe.id + '/readlater', null, UserService.getCurrentToken(), function (response) {
            Notification.success({message: response.data.message});
            recipe.users_readlater = !recipe.users_readlater;
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('BlogController', ['$scope', '$location', 'HttpGetServices', 'parseHtmlService', 'HttpPostServices', 'UserService', 'Notification', function ($scope, $location, HttpGetServices, parseHtmlService, HttpPostServices, UserService, Notification) {
    $scope.nextUrl = null;
    $scope.loaded = false;
    $scope.loadedMore = true;
    $scope.blogs = [];
    $scope.disableBtn = false;
    $scope.page = 2;
    $scope.loadedBlog = false;
    HttpGetServices.getListData(HttpGetServices.endpointBlogCategories).then(function (response) {
        $scope.categories = response.entity;
    });
    $scope.updateView = function (url) {
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            angular.forEach(response, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
                $scope.blogs.push(blog);
            });
            $scope.loaded = true;
            $scope.disableBtn = false;
            $scope.page += 1;
            $scope.loadedBlog = true;
            $scope.loadedMore = true;

        });
    };
    $scope.loadMore = function () {
        $scope.loadedMore = false;

        var url = HttpGetServices.endpointBlogList + '?page=' + $scope.page;
        $scope.disableBtn = true;
        $scope.updateView(url);
    };
    HttpGetServices.getListData('/api/blog/featured').then(function (data) {
        $scope.firstFeatured = data.entity[0];
        $scope.secondFeatured = data.entity[1];
        $scope.updateView(HttpGetServices.endpointBlogList);
    });
    $scope.parseHtml = function (data) {
        return parseHtmlService.parse(data);
    };
    $scope.addToReadLater = function (entity, index) {
        var entityUrl = entity.thumbnail ? 'api/recipe/' : 'api/blog/';
        HttpPostServices.postData(entityUrl + entity.id + '/readlater', null, UserService.getCurrentToken(), function (response) {
            Notification.success({message: response.data.message});
            $scope.blogs[index].is_readlater = !entity.is_readlater;
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('BlogViewController', ['$scope', 'HttpGetServices', '$routeParams', 'Notification', '$location', 'HttpPostServices', 'UserService', function ($scope, HttpGetServices, $routeParams, Notification, $location, HttpPostServices, UserService) {
    $scope.id = $routeParams.blogId;
    $scope.loadedSingleBlog = false;
    $scope.disqusConfig = {
        disqus_shortname: 'chef-g',
        disqus_identifier: 'single-blog-' + $scope.id,
        disqus_url: $location.absUrl()
    };
    $scope.addToReadLater = function (blog) {
        var readLaterData = {
            featured: true
        };
        HttpPostServices.postData(HttpPostServices.endpointBlogFeature + blog.id + '/readlater', readLaterData, UserService.getCurrentToken(), function (response) {
            Notification.success({message: 'Successfully updated users "Read Later" list.'});
            blog.users_readlater = !blog.users_readlater;
        }, function (error) {
        });
    };
    HttpGetServices.getWithToken(HttpGetServices.endpointAdvertisement, UserService.getCurrentToken()).then(function(data) {
        $scope.ad = data.entity;
    });
    $scope.shareUrl = $location.absUrl();
    HttpGetServices.getWithToken(HttpGetServices.endpointBlogList + '/' + $routeParams.blogId, UserService.getCurrentToken()).then(function (response) {
        $scope.blog = response.entity;
        $scope.loadedSingleBlog = true;
        console.log(response.entity);
        $scope.blog.created_at = Date.parse($scope.blog.created_at);
    });
}]);
myAppControllers.controller('ReadLaterController', ['$scope', 'HttpGetServices', 'HttpPostServices', 'UserService', 'Notification', function ($scope, HttpGetServices, HttpPostServices, UserService, Notification) {
    $scope.blogs = [];
    $scope.recipes = [];
    $scope.loadedReadLater = false;
    $scope.updateReadLater = function () {
        if (UserService.getUserId()) {
            HttpGetServices.getWithToken('api/user/' + UserService.getUserId(), UserService.getCurrentToken()).then(function (response) {
                console.log(response.entity);
                angular.forEach(response.entity.read_later, function (value) {
                    if (value.entity) {
                        value.entity.created_at = Date.parse(value.entity.created_at);
                    }
                    if (value.entity_type == 'App\\Blog') {
                        $scope.blogs.push(value.entity);
                    } else {
                        $scope.recipes.push(value.entity);
                    }
                });
                $scope.loadedReadLater = true;
            });
        } else {
            $scope.loadedReadLater = true;
        }

    };
    $scope.updateReadLater();
    $scope.removeBlog = function (blog) {
        HttpPostServices.postData('/api/blog/' + blog.id + '/readlater', null, UserService.getCurrentToken(), function (response) {
            Notification.success({message: 'Successfully removed blog!'});
            var index = $scope.blogs.indexOf(blog);
            if (index != -1) {
                $scope.blogs.splice(index, 1);
            }
            if ($scope.blogs.length == 0 && $scope.recipes.length == 0) {
                $scope.isEmptyReadLater = false;
            }
        }, function (error) {
        });
    };
    $scope.removeRecipe = function (recipe) {
        HttpPostServices.postData('/api/recipe/' + recipe.id + '/readlater', null, UserService.getCurrentToken(), function (response) {
            Notification.success({message: 'Successfully removed recipe!'});
            var index = $scope.recipes.indexOf(recipe);
            if (index != -1) {
                $scope.recipes.splice(index, 1);
            }
            if ($scope.blogs.length == 0 && $scope.recipes.length == 0) {
                $scope.isEmptyReadLater = false;
            }
        }, function (error) {
        });
    };

}]);
myAppControllers.controller('WishListController', ['$timeout', '$scope', 'HttpPostServices', 'HttpGetServices', 'localStorageService', '$location', 'UserService', 'Notification', function ($timeout, $scope, HttpPostServices, HttpGetServices, localStorageService, $location, UserService, Notification) {
    $scope.loadedWish = false;
    $scope.set_color = function (wishlist) {
        return (wishlist.user_liked) ? {color: "#ff4949"} : {};
    };

    $scope.updateWishList = function () {
        HttpGetServices.getWithToken(HttpGetServices.endpointWishListWithUser, UserService.getCurrentToken()).then(function (data) {
            $scope.wishes = data.entity;
            $scope.loadedWish = true;
        });
    };

    $scope.updateWishList();

    $scope.sendWishList = function () {
        if ($scope.wishlistData.title == '' || $scope.wishlistData.content == '') {
        } else {
            HttpPostServices.postData(HttpPostServices.endpointAddWishList, $scope.wishlistData, '', function (response) {
                Notification.success({message: 'Successfully sent wishlist!'});
                $scope.updateWishList();
            }, function (error) {
            });
        }
    };
    $scope.likeWishlist = function (wishlistId) {
        HttpPostServices.postData(HttpPostServices.endpointAddWishList + wishlistId + '/update', null, UserService.getCurrentToken(), function (response) {
            $scope.updateWishList();
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('NotFoundPageController', ['$scope', 'Notification', function ($scope, Notification) {
}]);
myAppControllers.controller('SearchResultsController', ['$scope', 'Notification', '$rootScope', '$location', 'HttpPostServices', 'UserService', function ($scope, Notification, $rootScope, $location, HttpPostServices, UserService) {
    $scope.addToReadLaterBlog = function (blogId) {
        HttpPostServices.postData('/api/blog/' + blogId + '/readlater', null, UserService.getCurrentToken(), function (response) {
            if (response.data[0] == 'Blog is unmarked.') {
                Notification.success({message: 'Successfully removed from "Read Later"'});
            } else {
                Notification.success({message: 'Successfully added to "Read Later"'});
            }
        }, function (error) {
        });
    };
    $scope.calculateClasses = function () {
        if ($rootScope.searchResult[1].length > 0 && $rootScope.searchResult[0].length > 0) {
            return "col-md-6 blogs-search";
        }
        return "col-md-12 blogs-search";
    };
    $scope.addToReadLaterRecipe = function (recipeId) {
        HttpPostServices.postData('api/recipe/' + recipeId + '/readlater', null, UserService.getCurrentToken(), function (response) {
            if (response.data[0] == 'Recipes is unmarked.') {
                Notification.success({message: 'Successfully removed from "Read Later"'});
            } else {
                Notification.success({message: 'Successfully added to "Read Later"'});
            }
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('EditRecipeController', ['$scope', 'HttpGetServices', 'FileUploader', 'UserService', '$routeParams', 'HttpPostServices', '$http', 'Notification', 'HttpUpdateServices', '$location', '$q', function ($scope, HttpGetServices, FileUploader, UserService, $routeParams, HttpPostServices, $http, Notification, HttpUpdateServices, $location, $q) {
    //Uploaders array
    var uploaders = $scope.uploaders = [];
    //ThumbUploader
    var thumbUpload = $scope.thumbUpload = new FileUploader({
        url: HttpPostServices.endpointUpdateRecipe,
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    //After recipe file upload, start submitting steps
    $scope.thumbUpload.onCompleteItem = function (item) {
        $scope.submitStep($scope.globalIndex);
    };
    //Update steps array to iterate through
    $scope.updatedSteps = [];
    //Global index to iterate through
    $scope.globalIndex = 0;
    //Setting headers
    $scope.headers = {
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    };
    //Setting file preview
    $scope.setFile = function (element, obj) {
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            obj.uploaded_image = event.target.result;
            obj.isFileNew = true;
            $scope.$apply();
        };
        reader.readAsDataURL(element.files[0]);
    };
    //Fetching recipe
    HttpGetServices.getSingleData(HttpGetServices.endpointGetSingleRecipe, $routeParams.recipeId).then(function (response) {
        $scope.currentRecipe = response.entity;
        $scope.currentRecipe.ingredients = $("<p>" + $scope.currentRecipe.ingredients + "</p>").text();
        angular.forEach($scope.currentRecipe.steps, function (step, key) {
            $scope.currentStep = step;
            $scope.uploaders[key] = new FileUploader({
                method: 'POST', //CHANGED PUT to Post
                url: 'api/step',
                headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
            });
            $scope.uploaders[key].onAfterAddingFile = function (item) {
                var step = {
                    recipe_id: $scope.currentRecipe.id,
                    id: $scope.currentStep.id,
                    step_key: key
                };
                item.url = '/api/step/' + $scope.currentStep.id + '/upload';
                item.formData = [];
                item.formData.push({'step': step});
            };
            $scope.uploaders[key].onCompleteItem = function (item, response, status, headers) {
                $scope.uploaders[$scope.globalIndex].clearQueue();
                $scope.globalIndex += 1;
                $scope.uploadPicture($scope.globalIndex);
            };
        });
    });
    //Adding step
    $scope.addStep = function () {
        var current = $scope.currentRecipe.steps.length;
        var next = $scope.currentRecipe.steps.length + 1;
        var obj = {
            title: null,
            step_number: next,
            description: null,
            recipe_id: $scope.currentRecipe.id
        };
        $scope.uploaders[current] = new FileUploader({
            method: 'POST', //CHANGED PUT to Post
            url: 'api/step',
            headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
        });
        $scope.uploaders[current].onAfterAddingFile = function (item) {
            var step = {
                recipe_id: $scope.currentRecipe.id
            };
            item.formData = [];
            item.formData.push({'step': step});
        };
        $scope.uploaders[current].onCompleteItem = function (item, response, status, headers) {
            $scope.uploaders[$scope.globalIndex].clearQueue();
            $scope.globalIndex += 1;
            $scope.uploadPicture($scope.globalIndex);
        };
        $scope.currentRecipe.steps.push(obj);
    };

    $scope.submitRecipe = function (isValid) {
        if (isValid) {
            var recipeUpdateUrl = HttpPostServices.endpointUpdateRecipe + $scope.currentRecipe.id + '/update';
            $scope.recipeUploadUrl = HttpPostServices.endpointUpdateRecipe + $scope.currentRecipe.id + '/upload';
            $http.post(recipeUpdateUrl, $scope.currentRecipe, $scope.headers).then(function (response) {
                var queue = $scope.thumbUpload.queue;
                var queueLength = queue.length;
                if (queueLength > 0) {
                    var lastIndex = queue.length - 1;
                    var file = queue[lastIndex];
                    file.url = $scope.recipeUploadUrl;
                    $scope.thumbUpload.uploadItem(file);
                } else {
                    $scope.submitStep($scope.globalIndex);
                }
            });
        }
    };

    $scope.submitStep = function (index) {
        if ($scope.currentRecipe.steps.length > index) {
            $scope.currentStep = $scope.currentRecipe.steps[index];
            var stepUpdateOrAddUrl = HttpPostServices.endpointAddRecipeSteps;
            if ($scope.currentStep.id != null) {
                stepUpdateOrAddUrl = HttpPostServices.endpointAddRecipeSteps + $scope.currentStep.id + '/update';
            }
            $http.post(stepUpdateOrAddUrl, $scope.currentStep, $scope.headers).then(function (response) {
                $scope.updatedSteps.push(response.data.entity);
                $scope.globalIndex += 1;
                $scope.submitStep($scope.globalIndex);
            });
        } else {
            $scope.globalIndex = 0;
            $scope.uploadPicture($scope.globalIndex);
        }
    };

    $scope.uploadPicture = function (index) {
        if ($scope.updatedSteps.length > index) {
            $scope.currentStep = $scope.updatedSteps[index];
            var stepPictureUpload = HttpPostServices.endpointAddRecipeSteps + $scope.currentStep.id + '/upload';
            $scope.currentUploader = $scope.uploaders[index];
            if ($scope.currentUploader.queue.length > 0) {
                $scope.currentPicture = $scope.currentUploader.queue[$scope.currentUploader.queue.length - 1];
                $scope.currentPicture.url = stepPictureUpload;
                $scope.currentUploader.uploadItem($scope.currentPicture);
            } else {
                $scope.globalIndex += 1;
                $scope.uploadPicture($scope.globalIndex);
            }
        }
        if ($scope.globalIndex == $scope.updatedSteps.length) {
            Notification.success({message: 'Successfully updated recipe.'});
        }
    };

}]);
myAppControllers.controller('BlogAdminController', ['$scope', '$location', 'HttpGetServices', 'FileUploader', 'UserService', 'parseHtmlService', 'Notification', 'HttpUpdateServices', 'HttpPostServices', 'HttpDeleteServices', '$route', function ($scope, $location, HttpGetServices, FileUploader, UserService, parseHtmlService, Notification, HttpUpdateServices, HttpPostServices, HttpDeleteServices, $route) {
    $scope.blogEditInputArea = '';
    $scope.blogEditTitle = '';
    $scope.blogInputArea = '';
    $scope.blogTitle = '';
    $scope.oldImgVisible = true;
    $scope.searchInp = {
        searchText: ""
    };
    $scope.nextUrl = '';
    $scope.backUrl = '';
    $scope.page = {};
    $scope.page.perPage = 5;
    $scope.features = [];
    $scope.loadedFeatured = false;
    $scope.loadedBlogs = false;
    $scope.goToAddBlog = function () {
        $location.path('/admin/add-blog');
    };
    $scope.goToEditBlog = function (blogId) {
        $location.path('/admin/edit-blog/' + blogId);
    };
    $scope.$watch('page.perPage', function (val) {
        var url = HttpGetServices.endpointAdminBlogList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.hidePrevious = (response.prev_page_url == null);
            $scope.hideNext = (response.next_page_url == null);
            $scope.blogs = response.data;
            angular.forEach($scope.blogs, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
            });
        });
    });
    $scope.goNextPage = function () {
        var url = $scope.nextUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.blogs = response.data;
            angular.forEach($scope.blogs, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
            });
        });
    };
    $scope.goBackPage = function (backUrl) {
        var url = $scope.backUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.blogs = response.data;
            angular.forEach($scope.blogs, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
            });
        });
    };
    $scope.searchBlog = function () {
        var url = HttpGetServices.endpointAdminBlogList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.blogs = response.data;
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            angular.forEach($scope.blogs, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
            });
        });
    };
    $scope.setFile = function (element) {
        $scope.oldImgVisible = false;
        $scope.currentFile = element.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            $scope.image_source = event.target.result;
            $scope.$apply()
        };
        reader.readAsDataURL(element.files[0]);
    };
    $scope.addAsFeatured = function (blog) {
        var data = {
            featured: true
        };
        HttpPostServices.postData(HttpPostServices.endpointBlogFeature + blog.id + '/featured', data, UserService.getCurrentToken(), function (response) {
            if (response.status == 226) {
                Notification.warning({message: response.data.message});
            } else {
                Notification.success({message: response.data.message});
            }
            $scope.loadFeatured();
        }, function (error) {
        });
    };
    $scope.setFile2 = function (element) {
        $scope.oldImgVisible = false;
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            $scope.image_source2 = event.target.result;
            $scope.$apply();
        };
        reader.readAsDataURL(element.files[0]);
    };
    $scope.loadFeatured = function() {
        $scope.loadedFeatured = false;
        $scope.features = [];
        HttpGetServices.getListData(HttpGetServices.endpointGetFeatureBlog).then(function (responseTwo) {
            console.log(responseTwo);
            angular.forEach(responseTwo.entity, function (item) {
                item.created_at = Date.parse(item.created_at);
                $scope.features.push(item);
            });
            $scope.loadedFeatured = true;
        });
    };
    $scope.updateView = function () {
        $scope.features = [];
        $scope.loadedBlogs = false;
        HttpGetServices.getWithToken(HttpGetServices.endpointAdminBlogList, UserService.getCurrentToken()).then(function (response) {
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.blogs = response.data;
            angular.forEach($scope.blogs, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
            });
            $scope.loadFeatured();
            $scope.loadedBlogs = true;
        });
    };
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/blog/post/add',
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    $scope.deleteBlog = function (blogId) {
        $scope.loadedBlogs = false;
        HttpDeleteServices.deleteData(HttpDeleteServices.endpointDeleteBlog + blogId, UserService.getCurrentToken(), function (response) {
            Notification.success({message: 'Successfully deleted Blog!'});
            $scope.updateView();
        }, function (error) {
        });
    };
    $scope.delete = function (obj) {
        if(obj.content) {
            HttpDeleteServices.deleteData(HttpDeleteServices.endpointDeleteBlog + obj.id, UserService.getCurrentToken(), function (response) {
                Notification.success({message: 'Successfully deleted Blog!'});
                $scope.updateView();
            }, function (error) {
            });
        } else {
            HttpDeleteServices.deleteData(HttpDeleteServices.endpointDeleteRecepie + obj.id, UserService.getCurrentToken(), function (resposne) {
                Notification.success({message: 'Successfully deleted Recipe!'});
                $scope.updateView();
            }, function (error) {
            });
        }
    };
    $scope.goToEdit = function(obj) {
        if(obj.content) {
            $scope.goToEditBlog(obj.id);
        } else {
            $location.path('/admin/edit-recipe/' + obj.id);
        }
    };

    $scope.goToSingle = function(obj) {
        if(obj.content) {
            $scope.goToBlog(obj.id);
        } else {
            $location.path('/explore-view/' + obj.id);
        }
    };

    $scope.addBlog = function () {
        var arr = uploader.getNotUploadedItems();
        angular.forEach(arr, function (value, key) {
            uploader.onBeforeUploadItem = function (item) {
                item.url = 'api/blog/post/add';
                item.formData.push({title: $scope.blogTitle});
                item.formData.push({content: $scope.blogInputArea});
                item.formData.push({tags: [1, 4]});
            };
            uploader.uploadItem(value);
        });
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            Notification.success({message: 'Successfully added blog!'});
            $scope.uploader.clearQueue();
            $scope.blogInputArea = '';
            $scope.blogTitle = '';
            $scope.updateView();
        };
    };
    $scope.updateView();
    $scope.parseHtml = function (data) {
        return parseHtmlService.parse(data);
    };
    $scope.goToBlog = function (id) {
        $location.path('/explore-view/' + id);
    };
    $scope.clearBlog = function () {
        $scope.blogEditTitle = '';
        $scope.blogEditInputArea = '';
    };
    $scope.editBlog = function (id) {
        HttpGetServices.getSingleData(HttpGetServices.endpointBlogList, id).then(function (response) {
            $scope.currentBlog = response[0];
            $scope.blogEditInputArea = $scope.currentBlog.content;
            $scope.blogEditTitle = $scope.currentBlog.title;
            $scope.imageUrl = $scope.currentBlog.image_path;
        });
    };
    $scope.updateBlog = function () {
        var reqData = {
            file: null,
            title: $scope.blogEditTitle,
            post_content: $scope.blogEditInputArea,
            user_id: $scope.currentBlog.user_id,
            category_id: $scope.currentBlog.category_id
        };
        var arr = uploader.getNotUploadedItems();
        if (arr == 0) {
            HttpPostServices.postData(HttpPostServices.endpointUpdateBlog + $scope.currentBlog.id + '/update', reqData, UserService.getCurrentToken(), function (response) {
                Notification.success({message: 'Successfully updated blog!'});
                $scope.updateView();
            }, function (error) {
            });
        } else {
            angular.forEach(arr, function (value, key) {
                uploader.onBeforeUploadItem = function (item) {
                    item.url = HttpUpdateServices.endpointUpdateBlog + $scope.currentBlog.id + '/update';
                    item.formData.push({title: $scope.blogEditTitle});
                    item.formData.push({post_content: $scope.blogEditInputArea});
                    item.formData.push({user_id: $scope.currentBlog.user_id});
                    item.formData.push({category_id: $scope.currentBlog.category_id});
                };
                uploader.uploadItem(value);
            });
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                $scope.uploader.clearQueue();
                Notification.success({message: 'Successfully added blog!'});
                $scope.updateView();
            };
        }
    };

}]);
myAppControllers.controller('RecipesController', ['$scope', '$location', 'HttpGetServices', 'HttpPostServices', 'UserService', 'Notification', 'HttpUpdateServices', 'HttpDeleteServices', function ($scope, $location, HttpGetServices, HttpPostServices, UserService, Notification, HttpUpdateServices, HttpDeleteServices) {
    $scope.searchInp = {};
    $scope.recents = [];
    $scope.loadedRecent = false;
    $scope.loadedRecipes = false;
    $scope.recipeInput = {
        title: '',
        description: ''
    };
    $scope.steps = [];
    $scope.recipeEdit = {
        title: '',
        description: ''
    };
    $scope.goToAddRecipe = function () {
        $location.path('/admin/add-recipe');
    };
    $scope.searchInp = {
        searchText: ""
    };
    $scope.nextUrl = '';
    $scope.backUrl = '';
    $scope.page = {};
    $scope.page.perPage = 5;
    $scope.$watch('page.perPage', function (val) {
        var url = HttpGetServices.endpointRecipesList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.recipes = response.data;
            angular.forEach($scope.recipes, function (recipe) {
                recipe.created_at = Date.parse(recipe.created_at);
            });
        });

    });
    $scope.goNextPage = function () {
        var url = $scope.nextUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.recipes = response.data;
            angular.forEach($scope.recipes, function (recipe) {
                recipe.created_at = Date.parse(recipe.created_at);
            });
        });
    };
    $scope.goBackPage = function (backUrl) {
        var url = $scope.backUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.recipes = response.data;
            angular.forEach($scope.recipes, function (recipe) {
                recipe.created_at = Date.parse(recipe.created_at);
            });
        });
    };
    $scope.searchRecipe = function () {
        var url = HttpGetServices.endpointRecipesList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != "") {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.recipes = response.data;
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            angular.forEach($scope.recipes, function (recipe) {
                recipe.created_at = Date.parse(recipe.created_at);
            });
        });
    };
    $scope.addAsFeatured = function (recipe) {
        recipe.feature_content = true;
        var data = {
            featured: true
        };
        HttpPostServices.postData(HttpPostServices.endpointRecipeFeature + recipe.id + '/featured', data, UserService.getCurrentToken(), function (response) {
            console.log(response);
            if (response.status == 226) {
                Notification.warning({message: response.data.message});
            } else {
                Notification.success({message: response.data.message});
            }
        }, function (error) {
        });
    };
    $scope.updateView = function () {
        $scope.recents = [];
        HttpGetServices.getWithToken(HttpGetServices.endpointRecipesList, UserService.getCurrentToken()).then(function (response) {
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.recipes = response.data;
            angular.forEach($scope.recipes, function (recipe) {
                recipe.created_at = Date.parse(recipe.created_at);
            });
            HttpPostServices.postData(HttpPostServices.endpointLatestRecipes, null, UserService.getCurrentToken(), function (response) {
                angular.forEach(response.data.entity, function (value) {
                    value.created_at = Date.parse(value.created_at);
                    $scope.recents.push(value);
                });
                $scope.loadedRecent = true;
                $scope.loadedMore = true;
            }, function (error) {
            });
            $scope.loadedRecipes = true;
            $scope.loadedMore = true;

        });
    };
    $scope.updateView();
    $scope.createRecipe = function () {
        HttpPostServices.postData(HttpPostServices.endpointAddRecipe, $scope.recipeInput, UserService.getCurrentToken(), function (response) {
            $location.path('/admin/recipe-steps/' + response.data.recipes_id);
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            Notification.success({message: 'Successfully added Recipe!'});
        });
    };
    $scope.editRecipe = function (id) {
        $location.path('/admin/edit-recipe/' + id);
    };
    $scope.deleteRecipe = function (recipeId) {
        HttpDeleteServices.deleteData(HttpDeleteServices.endpointDeleteRecepie + recipeId, UserService.getCurrentToken(), function (resposne) {
            $scope.updateView();
            Notification.success({message: 'Successfully deleted Recipe!'});
        }, function (error) {
        });
    };
}]);
myAppControllers.controller('AdminDashboardController', ['$scope', 'Notification', 'HttpGetServices', 'UserService', function ($scope, Notification, HttpGetServices, UserService) {
    HttpGetServices.getWithToken(HttpGetServices.endpointGetDashboardData, UserService.getCurrentToken()).then(function (response) {
        if (response == 'token_expired') {
            Notification.error('Token Expired!');
            UserService.logout();
        }
        $scope.dashboarData = response.entity;
    });
}]);
myAppControllers.controller('AboutController', ['$scope', 'Notification', '$timeout', function ($scope, Notification, $timeout) {
    $scope.loadedAbout = false;
    $timeout(function () {
        $scope.loadedAbout = true;
    }, 100);
}]);
myAppControllers.controller('BlogsListController', ['$scope', 'Notification', '$routeParams', 'HttpGetServices', 'HttpPostServices', 'UserService', function ($scope, Notification, $routeParams, HttpGetServices, HttpPostServices, UserService) {
    $scope.categoryId = $routeParams.categoryId;
    $scope.nextUrl = '';
    $scope.categoryName = $routeParams.categoryName;
    $scope.blogs = [];
    $scope.hideLoadMore = false;
    $scope.page = 1;
    $scope.disableBtn = false;
    $scope.updateView = function (url) {
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (data) {
            if (data.length == 0) {
                $scope.hideLoadMore = true;
            }
            $scope.page += 1;
            angular.forEach(data, function (blog) {
                blog.created_at = Date.parse(blog.created_at);
                $scope.blogs.push(blog);
            });
        });
    };

    $scope.updateView(HttpGetServices.endpointBlogsWithCat + $scope.categoryId + '?per_page=5&page=' + $scope.page);

    $scope.addToReadLater = function (entity, index) {
        HttpPostServices.postData('api/blog/' + entity.id + '/readlater', null, UserService.getCurrentToken(), function (response) {
            Notification.success({message: response.data.message});
            entity.users_readlater = !entity.users_readlater;
        }, function (error) {
        });
    };

    $scope.loadMore = function () {
        var url = HttpGetServices.endpointBlogsWithCat + $scope.categoryId + '?per_page=5&page=' + $scope.page;
        $scope.disableBtn = true;
        $scope.updateView(url);
    };
}]);
myAppControllers.controller('AddRecipeController', ['$scope', 'HttpGetServices', 'FileUploader', 'UserService', '$routeParams', 'HttpPostServices', '$http', 'Notification', '$location', function ($scope, HttpGetServices, FileUploader, UserService, $routeParams, HttpPostServices, $http, Notification, $location) {
    $scope.newRecipe = {
        title: '',
        description: '',
        ingredients: '',
        equipment: '',
        steps: []
    };
    //Updated recipe
    $scope.currentRecipe = {};
    //Uploaders array
    var uploaders = $scope.uploaders = [];
    //ThumbUploader
    var thumbUpload = $scope.thumbUpload = new FileUploader({
        url: HttpPostServices.endpointUpdateRecipe,
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    //After recipe file upload, start submitting steps
    $scope.thumbUpload.onCompleteItem = function (item) {
        $scope.submitStep($scope.globalIndex);
    };
    //Update steps array to iterate through
    $scope.updatedSteps = [];
    //Global index to iterate through
    $scope.globalIndex = 0;
    //Setting headers
    $scope.headers = {
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    };
    //Setting file preview
    $scope.setFile = function (element, obj) {
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            obj.uploaded_image = event.target.result;
            obj.isFileNew = true;
            $scope.$apply();
        };
        reader.readAsDataURL(element.files[0]);
    };
    //Adding step
    $scope.addStep = function () {
        var current = $scope.newRecipe.steps.length;
        var next = $scope.newRecipe.steps.length + 1;
        var obj = {
            title: null,
            step_number: next,
            description: null,
            recipe_id: null
        };
        $scope.uploaders[current] = new FileUploader({
            method: 'POST', //CHANGED PUT to Post
            url: 'api/step',
            headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
        });
        $scope.uploaders[current].onAfterAddingFile = function (item) {
            var step = {
                recipe_id: null
            };
            item.formData = [];
            item.formData.push({'step': step});
        };
        $scope.uploaders[current].onCompleteItem = function (item, response, status, headers) {
            $scope.uploaders[$scope.globalIndex].clearQueue();
            $scope.globalIndex += 1;
            $scope.uploadPicture($scope.globalIndex);
        };
        $scope.newRecipe.steps.push(obj);
    };

    $scope.submitRecipe = function (isValid) {
        if (isValid) {
            var recipeAddUrl = HttpPostServices.endpointUpdateRecipe;
            $http.post(recipeAddUrl, $scope.newRecipe, $scope.headers).then(function (response) {
                $scope.currentRecipe = response.data.entity;
                $scope.recipeUploadUrl = HttpPostServices.endpointUpdateRecipe + $scope.currentRecipe.id + '/upload';
                var queue = $scope.thumbUpload.queue;
                var queueLength = queue.length;
                if (queueLength > 0) {
                    var lastIndex = queue.length - 1;
                    var file = queue[lastIndex];
                    file.url = $scope.recipeUploadUrl;
                    $scope.thumbUpload.uploadItem(file);
                } else {
                    $scope.submitStep($scope.globalIndex);
                }
            });
        }
    };

    $scope.submitStep = function (index) {
        if ($scope.newRecipe.steps.length > index) {
            $scope.currentStep = $scope.newRecipe.steps[index];
            $scope.currentStep.recipe_id = $scope.currentRecipe.id;
            var stepUpdateOrAddUrl = HttpPostServices.endpointAddRecipeSteps;
            $http.post(stepUpdateOrAddUrl, $scope.currentStep, $scope.headers).then(function (response) {
                $scope.updatedSteps.push(response.data.entity);
                $scope.globalIndex += 1;
                $scope.submitStep($scope.globalIndex);
            });
        } else {
            $scope.globalIndex = 0;
            $scope.uploadPicture($scope.globalIndex);
        }
    };

    $scope.uploadPicture = function (index) {
        if ($scope.updatedSteps.length > index) {
            $scope.currentStep = $scope.updatedSteps[index];
            var stepPictureUpload = HttpPostServices.endpointAddRecipeSteps + $scope.currentStep.id + '/upload';
            $scope.currentUploader = $scope.uploaders[index];
            if ($scope.currentUploader.queue.length > 0) {
                $scope.currentPicture = $scope.currentUploader.queue[$scope.currentUploader.queue.length - 1];
                $scope.currentPicture.url = stepPictureUpload;
                $scope.currentUploader.uploadItem($scope.currentPicture);
            } else {
                $scope.globalIndex += 1;
                $scope.uploadPicture($scope.globalIndex);
            }
        }
        if ($scope.globalIndex == $scope.updatedSteps.length) {
            Notification.success({message: 'Successfully created recipe.'});
        }
    };
}]);
myAppControllers.controller('UsersAdminController', ['$scope', 'HttpGetServices', 'UserService', '$routeParams', '$http', '$location', '$rootScope', '$timeout', function ($scope, HttpGetServices, UserService, $routeParams, $http, $location, $rootScope, $timeout) {
    $scope.searchInp = {
        searchText: null
    };
    $scope.page = {
        perPage: 5,
        usersLoading: true
    };
    $scope.users = [];

    $scope.editUser = function (id) {
        $location.path('/admin/edit-admin/' + id);
    };
    $scope.searchUsers = function () {
        var url = HttpGetServices.endpointListUsers + "?per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.perPage = function (perPage) {
        $scope.page.perPage = perPage || 5;
        var url = HttpGetServices.endpointListUsers + "?per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.goNextPage = function (nextUrl) {
        var url = nextUrl + "&per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.goBackPage = function (prevUrl) {
        var url = prevUrl + "&per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.dispatch = function (url) {
        if ($scope.searchInp.searchText) {
            url += "&search=" + $scope.searchInp.searchText;
        }
        $scope.refreshList(url);
    };
    $scope.refreshList = function (url) {
        url = url || HttpGetServices.endpointListUsers + "?per_page=" + $scope.page.perPage;
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            $scope.pagination = response;
            $scope.users = response.data;
            $scope.hidePrevious = (response.prev_page_url != null);
            $scope.hideNext = (response.next_page_url != null);
            $scope.page.usersLoading = true;
        });
    };
    //Init list
    $scope.refreshList();
}]);
myAppControllers.controller('RegisterController', ['$scope', 'UserService', 'HttpPostServices', 'Notification', 'localStorageService', function ($scope, UserService, HttpPostServices, Notification, localStorageService) {
    $scope.firstName = null;
    $scope.lastName = null;
    $scope.email = null;
    $scope.password = null;
    $scope.repeatedPassword = null;
    $scope.phoneNumber = null;
    $scope.password_confirmation = null;
    $scope.register = function (isValid) {
        if (isValid) {
            if ($scope.password != $scope.password_confirmation) {
                Notification.warning('Passwords must match!');
            } else {
                var registerData = {
                    firstname: $scope.firstName,
                    lastname: $scope.lastName,
                    phonenumber: $scope.phoneNumber,
                    email: $scope.email,
                    password: $scope.password,
                    password_confirmation: $scope.password_confirmation
                }
                HttpPostServices.postData(HttpPostServices.registerUser, registerData, '', function (response) {
                    console.log(response);
                    // UserService
                    var entity = response.data.entity;
                    localStorageService.set('avatarPath', entity.avatar);
                    localStorageService.set('fullName', entity.firstname + ' ' + entity.lastname);
                    localStorageService.set('role', entity.role);
                    localStorageService.set('title', entity.title);
                    localStorageService.set('token', entity.token);
                    localStorageService.set('role', 'ROLE_USER');
                    Notification.success({message: response.data.message});
                    $scope.goToDashboard();
                }, function (error) {
                });
            }
        }
    };

}]);
myAppControllers.controller('ResetPasswordController', ['$scope', 'UserService', 'HttpPostServices', 'Notification', 'localStorageService', function ($scope, UserService, HttpPostServices, Notification, localStorageService) {
    $scope.email = '';
    $scope.resetPassword = function () {
        if ($scope.email == '') {
        } else {
            HttpPostServices.postData(HttpPostServices.resetPassword, {email: $scope.email}, '', function (response) {
                Notification.success('Check your email for password reset link!');
            }, function (error) {
            })
        }
    };

}]);
myAppControllers.controller('EditBlogController', ['$scope', '$location', 'HttpGetServices', 'FileUploader', 'UserService', 'parseHtmlService', 'Notification', 'HttpUpdateServices', 'HttpPostServices', 'HttpDeleteServices', '$route', '$routeParams', function ($scope, $location, HttpGetServices, FileUploader, UserService, parseHtmlService, Notification, HttpUpdateServices, HttpPostServices, HttpDeleteServices, $route, $routeParams) {
    $scope.tags = {};
    $scope.blogEditInputArea = {};
    $scope.blogEditTitle = {};
    $scope.availableTags = [];
    $scope.tags.all = $scope.tags.all ? $scope.tags.all : [];
    $scope.render = function(val) {
        if(val.id) {
            return val;
        }
        return { title: val }
    };
    $scope.setFile = function (element) {
        $scope.oldImgVisible = false;
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            $scope.image_source = event.target.result;
            $scope.$apply()
        };
        reader.readAsDataURL(element.files[0]);
    };
    HttpGetServices.getListData(HttpGetServices.endpointAllTags).then(function (response) {
        angular.forEach(response.entity, function (value) {
            $scope.availableTags.push(value);
        });
        HttpGetServices.getSingleData(HttpGetServices.endpointBlogList, $routeParams.blogId).then(function (data) {
            $scope.currentBlog = data.entity;
            $scope.blogEditInputArea.content = $scope.currentBlog.content;
            $scope.blogEditTitle.title = $scope.currentBlog.title;
            angular.forEach($scope.currentBlog.tags, function (value) {
                $scope.tags.all.push(value);
            });
            $scope.$watch('tags.all', function (val) {
                angular.forEach($scope.tags.all, function (tag) {
                    angular.forEach($scope.availableTags, function (avaTag, i) {
                        if (tag.id == avaTag.id) {
                            $scope.availableTags.splice(i, 1);
                        }
                    });
                });
            });
        });
    });
    var uploader = $scope.uploader = new FileUploader({
        method: 'POST', // Changed PUT to POST
        url: 'api/blog/',
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    uploader.onAfterAddingFile = function (fileItem) {
        uploader.queue = [];
        uploader.queue[0] = fileItem;
    };
    $scope.updateBlog = function (isValid) {
        if (isValid) {
            var reqData = {
                title: $scope.blogEditTitle.title,
                content: $scope.blogEditInputArea.content,
                tags: $scope.tags.all
            };
            HttpUpdateServices.updateData(HttpUpdateServices.endpointUpdateBlog + $scope.currentBlog.id + '/update', reqData, UserService.getCurrentToken(), function (response) {
                var arr = uploader.queue;
                if (arr.length > 0) {
                    var lastFileUploaded = arr[0];
                    uploader.onBeforeUploadItem = function (item) {
                        item.url = HttpUpdateServices.endpointUpdateBlog + response.data.entity.id + '/upload';
                    };
                    uploader.onCompleteItem = function (fileItem, response, status, headers) {
                        Notification.success({message: 'Successfully updated blog!'});
                        $scope.uploader.clearQueue();
                        $location.path('/admin/blog');
                    };
                    uploader.uploadItem(lastFileUploaded);
                } else {
                    Notification.success({message: 'Successfully updated blog!'});
                    $location.path('/admin/blog');
                }
            }, function (error) {
            });
        }
    };
}]);
myAppControllers.controller('AddBlogController', ['$scope', '$location', 'HttpGetServices', 'FileUploader', 'UserService', 'parseHtmlService', 'Notification', 'HttpUpdateServices', 'HttpPostServices', 'HttpDeleteServices', '$route', function ($scope, $location, HttpGetServices, FileUploader, UserService, parseHtmlService, Notification, HttpUpdateServices, HttpPostServices, HttpDeleteServices, $route) {
    $scope.blogInputArea = '';
    $scope.blogTitle = '';
    $scope.tags = [];
    $scope.availableTags = [];
    HttpGetServices.getListData(HttpGetServices.endpointAllTags).then(function (response) {
        angular.forEach(response.entity, function (value) {
            $scope.availableTags.push(value);
        });
    });
    $scope.render = function(val) {
        if(val.id) {
            return val;
        }
        return { title: val }
    };
    $scope.$watch('tags', function (val) {
        angular.forEach($scope.tags, function (tag) {
            angular.forEach($scope.availableTags, function (avaTag, i) {
                if (tag.id == avaTag.id) {
                    $scope.availableTags.splice(i, 1);
                }
            });
        });
    });
    $scope.setFile2 = function (element) {
        $scope.oldImgVisible = false;
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            $scope.image_source2 = event.target.result;
            $scope.$apply();
        };
        reader.readAsDataURL(element.files[0]);
    };
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/blog',
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    uploader.onAfterAddingFile = function (fileItem) {
        uploader.queue = [];
        uploader.queue[0] = fileItem;
    };
    $scope.addBlog = function (isValid) {
        if (isValid) {
            var reqData = {
                title: $scope.blogTitle,
                content: $scope.blogInputArea,
                tags: $scope.tags
            };
            HttpPostServices.postData(HttpPostServices.endpointAddBlog, reqData, UserService.getCurrentToken(), function (response) {
                var arr = uploader.queue;
                if (arr.length > 0) {
                    var lastFileUploaded = arr[0];
                    uploader.onBeforeUploadItem = function (item) {
                        item.url = HttpUpdateServices.endpointUpdateBlog + response.data.entity.id + '/upload';
                    };
                    uploader.onCompleteItem = function (fileItem, response, status, headers) {
                        Notification.success({message: 'Successfully created blog!'});
                        uploader.clearQueue();
                        $location.path('/admin/blog');
                    };
                    uploader.uploadItem(lastFileUploaded);
                } else {
                    Notification.success({message: 'Successfully created blog!'});
                    $location.path('/admin/blog');
                }
            }, function (error) {
            });
        }
    };
}]);

myAppControllers.controller('TagsCategoriesController', ['$scope', 'HttpGetServices', 'UserService', '$location', function ($scope, HttpGetServices, UserService, $location) {
    $scope.searchInp = {
        searchText: null
    };
    $scope.page = {
        perPage: 5
    };
    $scope.hideNext = false;
    $scope.hidePrevious = false;
    $scope.tags = [];
    $scope.categories = [];
    $scope.loadedCategoriesTags = false;
    $scope.searchTags = function () {
        var url = HttpGetServices.endpointLatestTags + "?per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.$watch('page.perPage', function (val) {
        var url = HttpGetServices.endpointLatestTags + "?per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    });
    $scope.goNextPage = function () {
        var url = $scope.nextUrl + "&per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.goBackPage = function (backUrl) {
        var url = $scope.backUrl + "&per_page=" + $scope.page.perPage;
        $scope.dispatch(url);
    };
    $scope.dispatch = function (url) {
        if ($scope.searchInp.searchText) {
            url += "&search=" + $scope.searchInp.searchText;
        }
        $scope.refreshList(url);
    };
    $scope.refreshList = function (url) {
        $scope.loadedCategoriesTags = false;
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            $scope.tags = [];
            if(!($scope.categories.length>0)) {
                $scope.categories = [];
                angular.forEach(response.data, function (value, key) {
                    if (key < 3) {
                        $scope.categories.push(value);
                    }
                });
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.hidePrevious = (response.prev_page_url == null);
            $scope.hideNext = (response.next_page_url == null);
            angular.forEach(response.data, function (value, key) {
                $scope.tags.push(value);
            });
            $scope.loadedCategoriesTags = true;
        });
    };
    $scope.goToEditTag = function (tagId) {
        $location.path('/admin/edit-tags/' + tagId);
    };
    $scope.goToBlogs = function (category) {
        $location.path('/blogs-list/' + category.id + '/' + category.title);
    };
}]);
myAppControllers.controller('ThankYouController', ['$scope', function ($scope) {

}]);
myAppControllers.controller('WishListAdminController', ['$scope', 'HttpGetServices', '$location', 'UserService', 'HttpPostServices', 'Notification', function ($scope, HttpGetServices, $location, UserService, HttpPostServices, Notification) {
    $scope.page = {};
    $scope.page.perPage = 5;
    $scope.nextUrl = '';
    $scope.backUrl = '';
    $scope.searchInp = {
        searchText: ""
    };
    $scope.loadedWishList = false;

    $scope.refreshWishList = function() {
        HttpGetServices.getWithToken('api/wish/admin/all' + "?per_page=" + $scope.page.perPage, UserService.getCurrentToken()).then(function (response) {
            $scope.wishlists = response.data;
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            angular.forEach($scope.wishlists, function (value) {
                value.created_at = Date.parse(value.created_at);
            });
            $scope.loadedWishList = true;
        });
    };
    $scope.refreshWishList();

    $scope.markResolved = function (wishlist) {
        HttpPostServices.postData(HttpPostServices.endpointResolveWish + wishlist.id + '/resolve', {}, UserService.getCurrentToken(), function(response) {
            Notification.success({message: 'Successfully resolved a wish.'});
            $scope.refreshWishList();
        });
    };

    $scope.searchwishlist = function () {
        var url = HttpGetServices.endpointWishList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != '') {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.wishlists = response.data;
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
        });
    };
    $scope.$watch('page.perPage', function (val) {
        var url = HttpGetServices.endpointWishList + "?per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != '') {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.wishlists = response.data;
        });
    }, true);
    $scope.goNextPage = function () {
        var url = $scope.nextUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != '') {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.wishlists = response.data;
        });
    };
    $scope.goBackPage = function (backUrl) {
        var url = $scope.backUrl + "&per_page=" + $scope.page.perPage;
        if ($scope.searchInp.searchText != '') {
            url += "&search=" + $scope.searchInp.searchText;
        }
        HttpGetServices.getWithToken(url, UserService.getCurrentToken()).then(function (response) {
            if (response.prev_page_url == null) {
                $scope.hidePrevious = true;
            } else {
                $scope.hidePrevious = false;
            }
            if (response.next_page_url == null) {
                $scope.hideNext = true;
            } else {
                $scope.hideNext = false;
            }
            $scope.nextUrl = response.next_page_url;
            $scope.backUrl = response.prev_page_url;
            $scope.wishlists = response.data;
        });
    };
    $scope.goToWishList = function (wishlist) {
        $location.path('/admin/wish-list/' + wishlist.id);
    }

}]);
myAppControllers.controller('WishListSingleAdminController', ['$scope', 'HttpGetServices', '$routeParams', '$location', 'UserService', 'HttpPostServices', 'Notification', function ($scope, HttpGetServices, $routeParams, $location, UserService, HttpPostServices, Notification) {
    HttpGetServices.getWithToken(HttpGetServices.endpointWishListWithUser + $routeParams.wishlistId, UserService.getCurrentToken()).then(function (response) {
        $scope.wishlist = response.entity;
        $scope.wishlist.created_at = Date.parse($scope.wishlist.created_at);
    });
    $scope.goBack = function () {
        $location.path('/admin/wish-list');
    };
    $scope.markResolved = function (wishlist) {
        HttpPostServices.postData(HttpPostServices.endpointResolveWish + wishlist.id + '/resolve', {}, UserService.getCurrentToken(), function(response) {
            Notification.success({message: 'Successfully resolved a wish.'});
            $scope.goBack();
        });
    };

}]);
myAppControllers.controller('AddAdminController', ['$scope', 'HttpPostServices', 'localStorageService', 'Notification', 'FileUploader', 'UserService', '$location', function ($scope, HttpPostServices, localStorageService, Notification, FileUploader, UserService, $location) {
    $scope.user = {};
    $scope.user.role = 'ROLE_USER';

    var uploader = $scope.uploader = new FileUploader({
        url: HttpPostServices.addUser,
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    uploader.onAfterAddingFile = function (fileItem) {
        uploader.queue = [];
        uploader.queue[0] = fileItem;
    };
    $scope.register = function (isValid) {
        if (isValid) {
            if ($scope.user.password != $scope.user.password_confirmation) {
                Notification.warning('Passwords must match!');
            } else {
                HttpPostServices.postData(HttpPostServices.addUser, $scope.user, UserService.getCurrentToken(), function (response) {
                    var arr = $scope.uploader.queue;
                    if (arr.length > 0) {
                        var lastFileUploaded = arr[0];
                        $scope.uploader.onBeforeUploadItem = function (item) {
                            item.url = 'api/user/' + response.data.entity.id + '/upload';
                        };
                        $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
                            Notification.success({message: 'Successfully added User!'});
                            $scope.uploader.clearQueue();
                            $scope.goToDashboard();
                        };
                        $scope.uploader.uploadItem(lastFileUploaded);
                    } else {
                        Notification.success({message: 'Successfully added User!'});
                        $scope.goToDashboard();
                    }
                }, function (error) {
                });
            }
        } else {
        }
    };

}]);
myAppControllers.controller('EditAdminController', ['$scope', '$routeParams', 'HttpGetServices', 'HttpPostServices', 'UserService', 'FileUploader', 'Notification', function ($scope, $routeParams, HttpGetServices, HttpPostServices, UserService, FileUploader, Notification) {
    $scope.user = [];
    HttpGetServices.getWithToken('api/user/' + $routeParams.userId, UserService.getCurrentToken()).then(function (response) {
        $scope.user = response.entity;
    });
    var uploader = $scope.uploader = new FileUploader({
        url: HttpPostServices.editUser,
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    $scope.uploader.onAfterAddingFile = function (fileItem) {
        uploader.queue = [];
        uploader.queue[0] = fileItem;
    };
    $scope.uploader.onCompleteItem = function (fileItem, responseTwo, status, headers) {
        $scope.getAuthUser();
    };
    $scope.getAuthUser = function() {
        HttpPostServices.postData(HttpPostServices.endpointAuthenticatedUser, {}, UserService.getCurrentToken(), function (responseThree) {
            Notification.success({message: 'Successfully updated User!'});
            UserService.setAuthenticatedUserData(responseThree.data.entity);
            $scope.uploader.clearQueue();
            $scope.goToDashboard();
        });
    };
    $scope.update = function (isValid) {
        if (isValid) {
            if ($scope.user.password != $scope.user.password_confirmation) {
                Notification.warning('Passwords must match!');
            } else {
                HttpPostServices.postData('api/user/' + $scope.user.id + '/update', $scope.user, UserService.getCurrentToken(), function (response) {
                    var arr = $scope.uploader.queue;
                    if (arr.length > 0) {
                        var lastFileUploaded = arr[0];
                        $scope.uploader.onBeforeUploadItem = function (item) {
                            item.url = 'api/user/' + response.data.entity.id + '/upload';
                        };
                        $scope.uploader.uploadItem(lastFileUploaded);
                    } else {
                        $scope.getAuthUser();
                        Notification.success({message: 'Successfully updated User!'});
                        $scope.goToDashboard();
                    }
                }, function (error) {
                    Notification.error({message: 'Internal server error.'});
                });
            }
        }
    };
}]);
myAppControllers.controller('AddTagsController', ['$scope', 'HttpPostServices', 'UserService', 'Notification', '$location', function ($scope, HttpPostServices, UserService, Notification, $location) {
    $scope.tags = [];
    $scope.render = function (val) {
        return {title: val}
    };
    $scope.addTags = function () {
        if ($scope.tags.length > 0) {
            HttpPostServices.postData(HttpPostServices.endpointAddTag, {'tags': $scope.tags}, UserService.getCurrentToken(), function (response) {
                Notification.success({message: 'Successfully created Tags!'});
                $location.path('/admin/tags-categories')
            }, function (error) {
            });
        }
    };
}]);
myAppControllers.controller('EditTagsController', ['$scope', '$routeParams', 'HttpGetServices', 'UserService', 'HttpPostServices', function ($scope, $routeParams, HttpGetServices, UserService, HttpPostServices) {
    HttpGetServices.getWithToken('/api/tag/' + $routeParams.tagId, UserService.getCurrentToken()).then(function (response) {
        $scope.tag = response.entity;
    });
    $scope.updateTag = function () {
        HttpPostServices.postData('/api/tag/' + $routeParams.tagId + '/update', $scope.tag, UserService.getCurrentToken(), function (response) {
        });
    };
}]);
myAppControllers.controller('SlidesController', ['$scope', 'FileUploader', 'HttpGetServices', 'UserService', '$location', 'HttpDeleteServices', 'Notification', 'HttpPostServices', function ($scope, FileUploader, HttpGetServices, UserService, $location, HttpDeleteServices, Notification, HttpPostServices) {
    $scope.page = {
        perPage: 5
    };
    $scope.updateList = function () {
        HttpGetServices.getWithToken('api/slide/admin/all', UserService.getCurrentToken()).then(function (response) {
            $scope.slides = response.data;
        });
    };
    $scope.updateList();
    $scope.deleteSlide = function (slide) {
        HttpDeleteServices.deleteData('api/slide/' + slide.id, UserService.getCurrentToken(), function (response) {
            $scope.updateList();
            Notification.success({message: 'Successfully deleted slide!'});
        }, function (error) {
        });
    };
    $scope.updateSlide = function (slide, index) {
        HttpPostServices.postData('api/slide/' + slide.id + '/update', {activated: !slide.activated}, UserService.getCurrentToken(), function (response) {
            $scope.updateList();
            Notification.success({message: 'Successfully updated slide!'});
        });
    };
}]);
myAppControllers.controller('AddSlidesController', ['$scope', 'UserService', 'FileUploader', '$location', 'Notification', 'HttpPostServices', 'HttpUpdateServices', function ($scope, UserService, FileUploader, $location, Notification, HttpPostServices, HttpUpdateServices) {
    $scope.checkbox = {
        active: false
    };
    $scope.image_source = '../../img/add-slider.png';
    var uploader = $scope.uploader = new FileUploader({
        url: 'ADD_SLIDE_URL',
        headers: {'Authorization': 'Bearer ' + UserService.getCurrentToken()}
    });
    //Don't remove this, this assures there is only one item in queue to be uploaded
    uploader.onAfterAddingFile = function (fileItem) {
        uploader.queue = [];
        uploader.queue[0] = fileItem;
    };
    $scope.setFile = function (element) {
        $scope.oldImgVisible = false;
        $scope.currentFile = element.files[0];
        var reader = new FileReader();
        reader.onload = function (event) {
            $scope.image_source = event.target.result;
            $scope.$apply()
        };
        reader.readAsDataURL(element.files[0]);
    };
    $scope.addSlide = function () {
        HttpPostServices.postData('/api/slide', {activated: $scope.checkbox.active}, UserService.getCurrentToken(), function (response) {
            var arr = uploader.queue;
            if (arr.length > 0) {
                var lastFileUploaded = arr[0];
                uploader.onBeforeUploadItem = function (item) {
                    item.url = HttpUpdateServices.endpointUpdateSlide + response.data.entity.id + '/upload';
                };
                uploader.onCompleteItem = function (fileItem, response, status, headers) {
                    Notification.success({message: 'Successfully added slide!'});
                    $scope.uploader.clearQueue();
                    $location.path('admin/slides');
                };
                uploader.uploadItem(lastFileUploaded);
            }
        });
    };
}]);
myAppControllers.controller('AdvertisementController', ['$scope', 'UserService', 'FileUploader', '$location', 'Notification', 'HttpPostServices', 'HttpUpdateServices', 'HttpGetServices', function ($scope, UserService, FileUploader, $location, Notification, HttpPostServices, HttpUpdateServices, HttpGetServices) {
    $scope.loadedAdvertisements = false;
    $scope.updateClicked = false;
    $scope.saveAdvertisement = function() {
        $scope.updateClicked = true;
        HttpUpdateServices.updateData(HttpUpdateServices.endpointAdvertisement + "/" + $scope.ad.id, $scope.ad, UserService.getCurrentToken(), function(response) {
            $scope.ad = response.data.entity;
            Notification.success({message: response.data.message});
            $scope.updateClicked = false;
        }).catch(function(error) {
            Notification.success({message: error});
            $scope.updateClicked = false;
        });
    };
    HttpGetServices.getWithToken(HttpGetServices.endpointAdvertisement, UserService.getCurrentToken()).then(function(data) {
        $scope.ad = data.entity;
        $scope.loadedAdvertisements = true;
    });
}]);

<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/20/17
 * Time: 06:23
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\FileHandler;
use App\Helpers\Constant;


class DirectoryCreator extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup application to properly work';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info("Building...");

        FileHandler::removeDir(Constant::absolutePath("IMAGES_PATH"));

        FileHandler::makeDir(Constant::absolutePath("BLOG_IMAGES_PATH"));
        FileHandler::makeDir(Constant::absolutePath("RECIPE_IMAGES_PATH"));
        FileHandler::makeDir(Constant::absolutePath("STEP_IMAGES_PATH"));
        FileHandler::makeDir(Constant::absolutePath("USER_IMAGES_PATH"));
        FileHandler::makeDir(Constant::absolutePath("SLIDE_IMAGES_PATH"));

        FileHandler::copy(public_path() . '/../resources/assets/images/2.jpg', Constant::absolutePath("BLOG_IMAGES_PATH") . '2.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/2.jpg', Constant::absolutePath("RECIPE_IMAGES_PATH") . '2.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/2.jpg', Constant::absolutePath("STEP_IMAGES_PATH") . '2.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        FileHandler::copy(public_path() . '/../resources/assets/images/4.jpg', Constant::absolutePath("BLOG_IMAGES_PATH") . '4.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/4.jpg', Constant::absolutePath("RECIPE_IMAGES_PATH") . '4.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/4.jpg', Constant::absolutePath("STEP_IMAGES_PATH") . '4.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        FileHandler::copy(public_path() . '/../resources/assets/images/5.jpg', Constant::absolutePath("BLOG_IMAGES_PATH") . '5.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/5.jpg', Constant::absolutePath("RECIPE_IMAGES_PATH") . '5.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/5.jpg', Constant::absolutePath("STEP_IMAGES_PATH") . '5.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        FileHandler::copy(public_path() . '/../resources/assets/images/6.png', Constant::absolutePath("BLOG_IMAGES_PATH") . '6.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/6.png', Constant::absolutePath("RECIPE_IMAGES_PATH") . '6.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/6.png', Constant::absolutePath("STEP_IMAGES_PATH") . '6.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        FileHandler::copy(public_path() . '/../resources/assets/images/alan_wake.png', Constant::absolutePath("SLIDE_IMAGES_PATH") . 'alan_wake.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/dmc.png', Constant::absolutePath("SLIDE_IMAGES_PATH") . 'dmc.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');
        FileHandler::copy(public_path() . '/../resources/assets/images/smoke_grenade.jpg', Constant::absolutePath("SLIDE_IMAGES_PATH") . 'smoke_grenade.jpg') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        FileHandler::copy(public_path() . '/../resources/assets/images/default.png', Constant::absolutePath("USER_IMAGES_PATH") . 'default.png') or $this->error('FAILED TO INSTALL DEPENDENCIES, CHECK \'resources/assets/images\' FOLDER EXISTS');

        $this->info("Build done.");
    }
}

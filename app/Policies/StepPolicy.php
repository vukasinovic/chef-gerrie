<?php

namespace App\Policies;

use App\Helpers\Constant;
use App\User;
use App\Step;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\File;

class StepPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the step.
     *
     * @param  \App\User  $user
     * @param  \App\Step  $step
     * @return mixed
     */
    public function view(User $user, Step $step)
    {
        return true;
    }

    /**
     * Determine whether the user can create steps.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("STEP_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can update the step.
     *
     * @param  \App\User  $user
     * @param  \App\Step  $step
     * @return mixed
     */
    public function update(User $user, Step $step)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("STEP_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can delete the step.
     *
     * @param  \App\User  $user
     * @param  \App\Step  $step
     * @return mixed
     */
    public function delete(User $user, Step $step)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("STEP_IMAGES_PATH"));
    }
}

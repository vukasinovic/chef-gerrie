<?php

namespace App\Policies;

use App\Helpers\Constant;
use App\User;
use App\Recipe;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\File;

class RecipePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the recipe.
     *
     * @param  \App\User  $user
     * @param  \App\Recipe  $recipe
     * @return mixed
     */
    public function view(User $user, Recipe $recipe)
    {
        return true;
    }

    /**
     * Determine whether the user can create recipes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("RECIPE_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can update the recipe.
     *
     * @param  \App\User  $user
     * @param  \App\Recipe  $recipe
     * @return mixed
     */
    public function update(User $user, Recipe $recipe)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("RECIPE_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can delete the recipe.
     *
     * @param  \App\User  $user
     * @param  \App\Recipe  $recipe
     * @return mixed
     */
    public function delete(User $user, Recipe $recipe)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("RECIPE_IMAGES_PATH"));
    }
}

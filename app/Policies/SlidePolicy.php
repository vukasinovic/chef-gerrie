<?php

namespace App\Policies;

use App\Helpers\Constant;
use App\User;
use App\Slide;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\File;

class SlidePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the slide.
     *
     * @param  \App\User  $user
     * @param  \App\Slide  $slide
     * @return mixed
     */
    public function view(User $user, Slide $slide)
    {
        return true;
    }

    /**
     * Determine whether the user can create slides.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("SLIDE_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can update the slide.
     *
     * @param  \App\User  $user
     * @param  \App\Slide  $slide
     * @return mixed
     */
    public function update(User $user, Slide $slide)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("SLIDE_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can delete the slide.
     *
     * @param  \App\User  $user
     * @param  \App\Slide  $slide
     * @return mixed
     */
    public function delete(User $user, Slide $slide)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("SLIDE_IMAGES_PATH"));
    }
}

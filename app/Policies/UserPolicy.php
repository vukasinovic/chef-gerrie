<?php

namespace App\Policies;

use App\Helpers\Constant;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param \App\User $userGoal
     * @param \App\User $user
     * @return bool
     */
    public function view(User $userGoal, User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF);
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $userGoal
     * @return bool
     */
    public function update(User $user, User $userGoal)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF);
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $userGoal
     * @return bool
     */
    public function delete(User $user, User $userGoal)
    {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF);
    }
}

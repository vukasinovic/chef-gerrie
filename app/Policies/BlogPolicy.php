<?php

namespace App\Policies;

use App\Helpers\Constant;
use App\User;
use App\Blog;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class BlogPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the blog.
     *
     * @param  \App\User $user
     * @param  \App\Blog $blog
     * @return boolean
     */
    public function view(User $user, Blog $blog) {
        return true;
    }

    /**
     * Determine whether the user can create blogs.
     *
     * @param  \App\User $user
     * @return boolean
     */
    public function create(User $user) {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("BLOG_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can update the blog.
     *
     * @param  \App\User $user
     * @param  \App\Blog $blog
     * @return boolean
     */
    public function update(User $user, Blog $blog) {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("BLOG_IMAGES_PATH"));
    }

    /**
     * Determine whether the user can delete the blog.
     *
     * @param  \App\User $user
     * @param  \App\Blog $blog
     * @return boolean
     */
    public function delete(User $user, Blog $blog) {
        return ($user->role == Constant::ROLE_ADMIN || $user->role == Constant::ROLE_STAFF) && File::exists(Constant::absolutePath("BLOG_IMAGES_PATH"));
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Blog' => 'App\Policies\BlogPolicy',
        'App\Recipe' => 'App\Policies\RecipePolicy',
        'App\Step' => 'App\Policies\StepPolicy',
        'App\Tag' => 'App\Policies\TagPolicy',
        'App\Wish' => 'App\Policies\WishPolicy',
        'App\Slide' => 'App\Policies\SlidePolicy',
        'App\User' => 'App\Policies\UserPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}

<?php

namespace App;

use App\Helpers\Constant;
use Illuminate\Database\Eloquent\Model;

class ReadLater extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'readlater';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity() {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function blogs() {
        return $this->morphToMany('App\Blog', 'entity', 'readlater', 'id', 'entity_id', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function recipes() {
        return $this->morphToMany('App\Recipe', 'entity', 'readlater', 'id', 'entity_id', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * @param $entity
     * @param User $user
     * @return bool
     */
    public function isCreated($entity, User $user) {
        return ($entity->id==$this->entity->id && get_class($this->entity)==get_class($entity) && $user->id==$this->user->id);
    }
}

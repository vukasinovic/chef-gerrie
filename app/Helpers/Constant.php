<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/20/17
 * Time: 00:05
 */

namespace App\Helpers;


class Constant {
    //Class identifiers
    const BLOG_IDENTIFIER = 'App\Blog';
    const RECIPE_IDENTIFIER = 'App\Recipe';
    const STEP_IDENTIFIER = 'App\Step';
    const USER_IDENTIFIER = 'App\User';
    const SLIDE_IDENTIFIER = 'App\Slide';
    const TAG_IDENTIFIER = 'App\Tag';
    const WISH_IDENTIFIER = 'App\Wish';
    //Roles
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_STAFF = 'ROLE_STAFF';
    //Assets routes
    const IMAGES_PATH = '/images/';
    const BLOG_IMAGES_PATH = '/images/blogs/';
    const RECIPE_IMAGES_PATH = '/images/recipes/';
    const STEP_IMAGES_PATH = '/images/steps/';
    const USER_IMAGES_PATH = '/images/users/';
    const SLIDE_IMAGES_PATH = '/images/slides/';
    const USER_DEFAULT_AVATAR_PATH = 'images/users/default.png';


    /**
     * @param $constant
     * @return string
     */
    public static function absolutePath($constant) {
        return public_path() . constant('self::' . $constant);
    }
}

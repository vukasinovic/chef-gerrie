<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/24/17
 * Time: 03:12
 */

namespace App\Http\Requests\Blog;


use App\Blog;
use App\Helpers\Constant;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeleteBlogRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $blog = $this->route('blog');
        return $blog && $user && $user->can('delete', Constant::BLOG_IDENTIFIER);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
        ];
    }
}
<?php

namespace App\Http\Requests\Blog;

use App\Blog;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateBlogRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $blog = $this->route('blog');
        return $blog && $user && $user->can('update', $blog);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'max:255',
            'content' => 'required',
            'tags' => 'sometimes|filled|array',
            'file' => 'sometimes|mimes:jpg,gif,jpeg,png|image'
        ];
    }
}

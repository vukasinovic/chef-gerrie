<?php

namespace App\Http\Requests\Blog;

use App\Helpers\Constant;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class CreateBlogRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        return $user->can('create', Constant::BLOG_IDENTIFIER);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:255',
            'content' => 'required',
            'tags' => 'sometimes|filled|array',
            'file' => 'sometimes|mimes:jpg,gif,jpeg,png|image'
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/24/17
 * Time: 03:24
 */

namespace App\Http\Requests\Recipe;


use App\Helpers\Constant;
use App\Recipe;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeleteRecipeRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $recipe = $this->route('recipe');
        return $recipe && $user && $user->can('delete', $recipe);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
        ];
    }
}
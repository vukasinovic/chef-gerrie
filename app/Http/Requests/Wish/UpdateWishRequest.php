<?php

namespace App\Http\Requests\Wish;

use App\Wish;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateWishRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $wish = $this->route('wish');
        return $user && $wish && $user->can('update', $wish);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
        ];
    }
}

<?php

namespace App\Http\Requests\Slide;

use App\Slide;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateSlideRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $slide = $this->route('slide');
        return $slide && $user && $user->can('update', $slide);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'activated' => 'sometimes|required|boolean'
        ];
    }
}

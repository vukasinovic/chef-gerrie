<?php

namespace App\Http\Requests\Tag;

use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\Constant;

class CreateTagRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        return $user->can('create', Constant::TAG_IDENTIFIER);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'tags' => 'required|array',
            'tags.*.title' => 'distinct|unique:tags,title|max:25'
        ];
    }
}

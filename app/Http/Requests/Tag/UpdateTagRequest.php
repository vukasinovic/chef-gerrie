<?php

namespace App\Http\Requests\Tag;

use App\Tag;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateTagRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $tag = $this->route('tag');
        return $tag && $user && $user->can('update', $tag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|unique:tags,title|max:25'
        ];
    }
}

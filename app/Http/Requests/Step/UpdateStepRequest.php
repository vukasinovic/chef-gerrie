<?php

namespace App\Http\Requests\Step;

use App\Step;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateStepRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $step = $this->route('step');
        return $step && $user && $user->can('update', $step);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:255',
            'description' => 'required',
            'file' => 'sometimes|mimes:jpg,gif,jpeg,png|image'
        ];
    }
}

<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\Constant;

class UpdateUserRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        $userToEdit = $this->route('user');
        return $userToEdit && $user && $user->can('update', $userToEdit);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => ['required', 'email', Rule::unique('users')->ignore($this->route('user')->id)],
            'password' => 'sometimes|required|confirmed|max:255',
            'role' => 'required|in:ROLE_USER,ROLE_STAFF,ROLE_ADMIN'
        ];
    }
}

<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phonenumber' => 'required|max:25',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|same:password_confirmation|max:255',
            'role' => 'sometimes|required|in:ROLE_USER,ROLE_STAFF,ROLE_ADMIN'
        ];
    }
}
<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\FileHandler;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\Step\CreateStepRequest;
use App\Http\Requests\Step\UpdateStepRequest;
use App\Recipe;
use App\Step;

class StepController extends Controller {
    /**
     * Default constructor
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Step\CreateStepRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStepRequest $request) {
        $recipe = Recipe::find($request->recipe_id);
        $step = new Step;
        $step->fill($request->all());
        $step->recipe()->associate($recipe);
        $step->save();
        return response()->json([
            'message' => 'Successfully added new step',
            'code' => 200,
            'entity' => $step
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Step $step
     * @return \Illuminate\Http\Response
     */
    public function show(Step $step) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Step $step
     * @return \Illuminate\Http\Response
     */
    public function edit(Step $step) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Step\UpdateStepRequest $request
     * @param  \App\Step $step
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStepRequest $request, Step $step) {
        $step->fill($request->all());
        $step->save();
        return response()->json([
            'message' => 'Successfully updated step',
            'code' => 200,
            'entity' => $step
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Step $step
     * @return \Illuminate\Http\Response
     */
    public function destroy(Step $step) {
        //
    }

    /**
     * Step file upload
     * @param ImageRequest $request
     * @param Step $step
     * @return \Illuminate\Http\JsonResponse
     */
    public function stepFileUpload(ImageRequest $request, Step $step) {
        $basePath = Constant::absolutePath("STEP_IMAGES_PATH");
        $file = $request->file('file');
        $fileHandler = new FileHandler;
        if ($file) {
            $step->thumbnail = Constant::STEP_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->removeFile($step->thumbnail)->first();
        }
        $step->save();
        return response()->json([
            'message' => 'Successfully uploaded a file',
            'code' => 200,
            'entity' => $step
        ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;

class AdvertisementController extends Controller
{
    /**
     * Default constructor
     */
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $ad = Advertisement::find(1);

        return response()->json([
            'message' => 'Successfully retrieved advertisement',
            'entity' => $ad,
            'code' => 200
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  Advertisement $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $ad) {
        $ad = Advertisement::find($request->get('id'));
        $ad->fill($request->all());
        $ad->save();

        return response()->json([
            'message' => 'Successfully updated advertisement',
            'entity' => $ad,
            'code' => 200
        ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Tymon\JWTAuth\Facades\JWTAuth;

class TagController extends Controller {
    /**
     * Default constructor
     */
    public function constructor() {
        $this->middleware('jwt.auth', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");

        if ($searchTerm == null) {
            $tags = Tag::withCount('blogs')->orderBy('blogs_count', 'desc')->paginate($pageCount);
        } else {
            $tags = Tag::withCount('blogs')->where('title', 'LIKE', '%' . $searchTerm . '%')->orderBy('blogs_count', 'desc')->paginate($pageCount);
        }

        return $tags;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Tag\CreateTagRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request) {
        $tags = $request->tags;
        foreach ($tags as $tag) {
            Tag::create($tag);
        }
        return response()->json([
            'message' => 'Successfully created tag.',
            'entity' => $tags,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag) {
        return response()->json([
            'message' => 'Successfully retrieved tag.',
            'entity' => $tag,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Tag\UpdateTagRequest $request
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag) {
        $tag->fill($request->all());
        $tag->save();
        return response()->json([
            'message' => 'Successfully updated tag.',
            'entity' => $tag,
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag) {
        //
    }

    /**
     * Categories for site
     * @param int $top
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories($top = 5) {
        $categories = Tag::withCount('blogs')->get()->sortByDesc('blogs_count')->take($top);
        return response()->json([
            'message' => 'Successfully retrieved categories.',
            'entity' => $categories,
            'code' => 200
        ], 200);
    }

    /**
     * Blogs for category
     * @param \Illuminate\Http\Request $request
     * @param Tag $tag
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function category(Request $request, Tag $tag) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $page = (empty($request->input("page"))) ? 1 : $request->input("page");

        $blogs = Blog::whereHas('tags', function ($query) use ($tag) {
            $query->where('id', '=', $tag->id);
        })->with('user')->get();

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $blogs->each(function ($curr, $key) use ($user) {
                $curr->users_readlater = $curr->read_later->contains('user_id', $user->id);
            });
        } catch (\Exception $e) {
        }

        $blogs = $blogs->sortByDesc('created_at')->forPage($page, $pageCount);

        return $blogs->values();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function allTags() {
        $tags = Tag::all();
        return response()->json([
            'message' => 'Successfully retrieved tags.',
            'entity' => $tags,
            'code' => 200
        ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\FileHandler;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Newsletter;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\AuthUserRequest;

class UserController extends Controller {
    /**
     * Default constructor
     */
    public function constructor() {
        $this->middleware('jwt.auth', ['except' => ['store', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");

        if ($searchTerm == null) {
            $users = User::orderBy('created_at', 'desc')->paginate($pageCount);
        } else {
            $users = User::where('firstname', 'LIKE', '%' . $searchTerm . '%')->orWhere('lastname', 'LIKE', '%' . $searchTerm . '%')->orWhere('email', 'LIKE', '%' . $searchTerm . '%')->orWhere('role', 'LIKE', '%' . $searchTerm . '%')->orderBy('created_at', 'desc')->paginate($pageCount);
        }

        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\User\CreateUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request) {
        $user = new User;
        $user->fill($request->all());
        $user->password = $request->password;
        $user->save();
        return response()->json([
            'message' => 'Successfully created new user',
            'entity' => $user,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        $user->load('read_later.entity.user');
        return response()->json([
            'message' => 'Successfully retrieved user',
            'entity' => $user,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\User\UpdateUserRequest $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user) {
        $user->fill($request->all());
        $user->save();
        return response()->json([
            'message' => 'Successfully updated user',
            'entity' => $user,
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        //
    }

    /**
     * User avatar upload
     * @param ImageRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function avatarFileUpload(ImageRequest $request, User $user) {
        $basePath = Constant::absolutePath("USER_IMAGES_PATH");
        $file = $request->file('file');
        $fileHandler = new FileHandler;
        if ($file) {
            if($user->avatar!=Constant::USER_DEFAULT_AVATAR_PATH) {
                $user->avatar = Constant::USER_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->removeFile($user->avatar)->first();
            } else {
                $user->avatar = Constant::USER_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->first();
            }
        }
        $user->save();
        return response()->json([
            'message' => 'Successfully uploaded a file',
            'code' => 200,
            'entity' => $user
        ], 200);
    }

    /**
     * Subscribe user for newsletter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request) {
        Newsletter::subscribeOrUpdate($request->email, ['FNAME' => $request->fname, 'LNAME' => $request->lname]);
        $message = 'You are successfully added to subscribers list.';
        $code = 200;
        if (Newsletter::getLastError()) {
            $message = Newsletter::getLastError();
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => null,
            'code' => $code
        ], $code);
    }

    /**
     * Retrieve authenticated user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticated() {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $message = 'Successfully retrieved authenticated user';
            $code = 200;
        } catch (\Exception $e) {
            $user = null;
            $message = $e->getMessage();
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => $user,
            'code' => $code
        ], $code);
    }

    /**
     * Authenticate user
     *
     * @param  \App\Http\Requests\User\AuthUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(AuthUserRequest $request) {
        try {
            $payload = $request->only('email', 'password');
            $token = JWTAuth::attempt($payload);
            if (!$token) {
                throw new \Exception('No matching credentials found.');
            } else {
                $entity = User::where('email', '=', $payload['email'])->first();
                $entity->token = $token;
                $message = 'Successfully authorized.';
                $code = 200;
            }
        } catch (\Exception $e) {
            $entity = null;
            $message = $e->getMessage();
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => $entity,
            'code' => $code
        ], $code);
    }

    /**
     * Registers new user
     *
     * @param  \App\Http\Requests\User\CreateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(CreateUserRequest $request) {
        try {
            $payload = $request->only('email', 'password');
            $entity = User::where('email', '=', $payload['email'])->where('password', '=', bcrypt($payload['password']))->first();
            if (!$found = $entity) {
                $entity = new User;
                $entity->fill($request->all());
                $entity->password = $request->password;
                if ($entity->save()) {
                    $token = JWTAuth::attempt($payload);
                    if ($token) {
                        $entity->token = $token;
                        $message = 'Successfully authorized.';
                        $code = 200;
                    }
                }
            }
            if ($found && (!isset($token) || !$token)) {
                throw new \Exception('Failed to register, try again later.');
            }
        } catch (\Exception $e) {
            $entity = null;
            $message = $e->getMessage() . ':' . $e->getLine();
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => $entity,
            'code' => $code
        ], $code);
    }
}

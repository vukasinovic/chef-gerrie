<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\FileHandler;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\Slide\CreateSlideRequest;
use App\Http\Requests\Slide\DeleteSlideRequest;
use App\Http\Requests\Slide\UpdateSlideRequest;
use Illuminate\Http\Request;
use App\Slide;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class SlideController extends Controller {

    /**
     * Default constructor
     */
    public function constructor() {
        $this->middleware('jwt.auth', ['except' => ['index', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator
     */
    public function index() {
        $slides = Slide::where('activated', true)->get()->sortByDesc('created_at');
        return response()->json([
            'message' => 'Successfully retrieved all slides',
            'entity' => $slides->values(),
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function create(Request $request) {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Slide\CreateSlideRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSlideRequest $request) {
        $slide = new Slide;
        $slide->activated = $request->activated;
        $slide->save();
        return response()->json([
            'message' => "Successfully created slide.",
            'entity' => $slide,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Slide $slide
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Slide $slide) {
        return response()->json([
            'message' => "Successfully retrieved an entity.",
            'entity' => $slide,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Slide $slide
     * @return void
     */
    public function edit(Slide $slide) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Slide\UpdateSlideRequest $request
     * @param  Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlideRequest $request, Slide $slide) {
        $slide->activated = $request->activated;
        $slide->save();
        return response()->json([
            'message' => 'Successfully updated slide.',
            'code' => 200,
            'entity' => $slide
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteSlideRequest $request
     * @param  Slide $slide
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteSlideRequest $request, Slide $slide) {
        $thumbnail = $slide->image_path;
        if ($slide->delete()) {
            $fileHandler = new FileHandler;
            $fileHandler->removeFile($thumbnail);
            $message = 'Successfully deleted recipe.';
            $code = 200;
        } else {
            $message = 'Internal server error';
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => null,
            'code' => $code
        ], $code);
    }

    /**
     * Display admin listing for a resource.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function adminIndex(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");

        $slides = Slide::orderBy('created_at')->paginate($pageCount);

        return $slides;
    }

    /**
     * Slide file upload
     * @param ImageRequest $request
     * @param Slide $slide
     * @return \Illuminate\Http\JsonResponse
     */
    public function slideFileUpload(ImageRequest $request, Slide $slide) {
        $basePath = Constant::absolutePath("SLIDE_IMAGES_PATH");
        $file = $request->file('file');
        $fileHandler = new FileHandler;
        if ($file) {
            $slide->image_path = Constant::SLIDE_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->removeFile($slide->image_path)->first();
        }
        $slide->save();
        return response()->json([
            'message' => 'Successfully uploaded a file',
            'code' => 200,
            'entity' => $slide
        ], 200);
    }
}

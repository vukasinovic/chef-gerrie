<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Helpers\Constant;
use App\Helpers\FileHandler;
use App\Http\Requests\Recipe\CreateRecipeRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\Recipe\DeleteRecipeRequest;
use App\Http\Requests\Recipe\UpdateRecipeRequest;
use App\ReadLater;
use App\Recipe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Tymon\JWTAuth\Facades\JWTAuth;

class RecipeController extends Controller {
    /**
     * Default constructor
     */
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['show', 'featured']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index() {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Recipe\CreateRecipeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRecipeRequest $request) {
        $user = JWTAuth::parseToken()->authenticate($request->get('X-Auth-Token'));
        $recipe = new Recipe;
        $recipe->fill($request->all());
        $recipe->user()->associate($user);
        $recipe->save();
        return response()->json([
            'message' => 'Successfully added new recipe',
            'entity' => $recipe,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe) {
        $recipe->load(['user', 'steps' => function($query) {
            $query->orderBy('step_number');
        }]);

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $recipe->users_readlater = $recipe->read_later->contains('user_id', $user->id);
        } catch(\Exception $e) {
        }

        return response()->json([
            'message' => 'Successfully retrieved an entity.',
            'entity' => $recipe,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Recipe\UpdateRecipeRequest $request
     * @param  Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRecipeRequest $request, Recipe $recipe) {
        $recipe->fill($request->all());
        $recipe->save();
        return response()->json([
            'message' => 'Successfully edited recipe',
            'entity' => $recipe,
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Recipe $recipe
     * @param  DeleteRecipeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteRecipeRequest $request, Recipe $recipe) {
        $thumbnail = $recipe->thumbnail;
        if ($recipe->delete()) {
            $fileHandler = new FileHandler;
            $fileHandler->removeFile($thumbnail);
            $message = 'Successfully deleted recipe.';
            $code = 200;
        } else {
            $message = 'Internal server error';
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => null,
            'code' => $code
        ], $code);
    }

    /**
     * Latest added blogs
     * @return \Illuminate\Http\JsonResponse
     */
    public function latest() {
        $latest = Recipe::orderBy('created_at', 'desc')->take(3)->get();
        return response()->json([
            'message' => 'Successfully retrieved an entity',
            'code' => 200,
            'entity' => $latest->values()
        ], 200);
    }

    /**
     * All blogs for admin
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function adminRecipes(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");

        if ($searchTerm == null) {
            $recipes = Recipe::with(['user', 'steps', 'read_later'])->orderBy('created_at', 'desc')->paginate($pageCount);
        } else {
            $recipes = Recipe::with(['user', 'steps', 'read_later'])->where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere('description', 'LIKE', '%' . $searchTerm . '%')->orderBy('created_at', 'desc')->paginate($pageCount);
        }

        return $recipes;
    }

    /**
     * Toggle featured for blog
     * @param Request $request
     * @param Recipe $recipe
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleFeatured(Request $request, Recipe $recipe) {
        $featuredRecipes = Recipe::where('featured', true)->get()->sortBy('featured_date');
        $featuredBlogs = Blog::where('featured', true)->get()->sortBy('featured_date')->each(function ($item, $key) use ($featuredRecipes) {
            $featuredRecipes->push($item);
        });
        $featuredRecipes = $featuredRecipes->sortBy('featured_date');
        $old = $featuredRecipes->first();
        $count = $featuredRecipes->count();
        if ($request->get('featured')) {
            if ($count == 2) {
                $old->featured = false;
                $old->featured_date = null;
                $old->save();
            } else if ($count > 2) {
                $toUnfeature = $featuredRecipes->sortBy('featured_date')->slice(2);
                if (!$toUnfeature->isEmpty()) {
                    $toUnfeature->each(function ($item, $key) {
                        $item->featured = false;
                        $item->featured_date = null;
                        $item->save();
                    });
                }
            }
            $recipe->featured = true;
            $recipe->featured_date = Carbon::now();

            $toRet = [
                'message' => 'Successfully added Recipe as Featured.',
                'code' => 200,
                'entity' => null
            ];
        } else {
            $recipe->featured = false;
            $recipe->featured_date = null;
            $toRet = [
                'message' => 'Successfully removed Recipe as Featured.',
                'code' => 200,
                'entity' => null
            ];
        }
        $recipe->save();
        return response()->json($toRet, 200);
    }

    /**
     * Toggle readLater for recipe
     * @param Request $request
     * @param Recipe $recipe
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleReadlater(Request $request, Recipe $recipe) {
        $user = JWTAuth::parseToken()->authenticate($request->get('X-Auth-Token'));

        $readLater = new ReadLater;
        $readLater->user()->associate($user);
        $readLater->entity()->associate($recipe);
        $exists = ReadLater::where('user_id', '=', $user->id)->where('entity_type', '=', Constant::RECIPE_IDENTIFIER)->where('entity_id', '=', $recipe->id)->first();
        if ($exists) {
            $exists->delete();
        } else {
            $readLater->save();
        }
        return response()->json([
            'message' => 'Successfully updated read later list',
            'code' => 200,
            'entity' => $user->load('read_later')
        ], 200);
    }

    /**
     * Recipe file upload
     * @param ImageRequest $request
     * @param Recipe $recipe
     * @return \Illuminate\Http\JsonResponse
     */
    public function recipeFileUpload(ImageRequest $request, Recipe $recipe) {
        $basePath = Constant::absolutePath("RECIPE_IMAGES_PATH");
        $file = $request->file('file');
        $fileHandler = new FileHandler;
        if ($file) {
            $recipe->thumbnail = Constant::RECIPE_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->removeFile($recipe->thumbnail)->first();
        }
        $recipe->save();
        return response()->json([
            'message' => 'Successfully uploaded a file',
            'code' => 200,
            'entity' => $recipe
        ], 200);
    }
}

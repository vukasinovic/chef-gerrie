<?php

namespace App\Http\Controllers;

use App\Http\Requests\Wish\CreateWishRequest;
use App\Http\Requests\Wish\UpdateWishRequest;
use App\User;
use App\Wish;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class WishController extends Controller {
    /**
     * Default constructor
     */
    public function constructor() {
        $this->middleware('jwt.auth', ['except' => ['index', 'store', 'update', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $wishes = Wish::where('active', true)->withCount('users_liked')->with(['user'])->orderBy('users_liked_count', 'desc')->take(5)->get();

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $wishes->each(function($current, $key) use ($user) {
                $current->user_liked = $current->users_liked->contains($user);
            });
        } catch(\Exception $e) {
        }

        return response()->json([
            'message' => 'Successfully retrieved wishes.',
            'entity' => $wishes->values(),
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Wish\CreateWishRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWishRequest $request) {
        $wish = new Wish;
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $wish->user()->associate($user);
        } catch (\Exception $e) {
        }
        $wish->fill($request->all());
        $wish->save();
        return response()->json([
            'message' => 'Successfully retrieved wishes.',
            'entity' => $wish,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wish $wish
     * @return \Illuminate\Http\Response
     */
    public function show(Wish $wish) {
        $wish->load(['user']);
        $wish->users_liked_count = $wish->users_liked()->count();
        return response()->json([
            'message' => 'Successfully retrieved wish.',
            'entity' => $wish,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wish $wish
     * @return \Illuminate\Http\Response
     */
    public function edit(Wish $wish) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Wish\UpdateWishRequest $request
     * @param  \App\Wish $wish
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWishRequest $request, Wish $wish) {
        $wish->fill($request->all());
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $wish->users_liked()->toggle($user);
            $wish->save();
            $wish->load(['users_liked'])->withCount('users_liked');
            $wish->user_liked = $wish->users_liked->contains($user);
        } catch(\Exception $e) {
        }
        return response()->json([
            'message' => 'Successfully updated wish.',
            'entity' => $wish,
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wish $wish
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wish $wish) {
        //
    }

    /**
     * Display admin listing for a resource.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function adminIndex(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");

        if ($searchTerm == null) {
            $wishes = Wish::where('active', true)->with(['users_liked', 'user'])->withCount('users_liked')->orderBy('users_liked_count', 'desc')->paginate($pageCount);
        } else {
            $wishes = Wish::where('active', true)->where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere('content', 'LIKE', '%' . $searchTerm . '%')->with(['users_liked', 'user'])->withCount('users_liked')->orderBy('users_liked_count', 'desc')->paginate($pageCount);
        }

        return $wishes;
    }

    /**
     * @param Request $request
     * @param Wish $wish
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolveWish(Request $request, Wish $wish) {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $wish->active = !$wish->active;
            $wish->save();
        } catch(\Exception $e) {
        }
        return response()->json([
            'message' => 'Successfully updated wish.',
            'entity' => $wish,
            'code' => 200
        ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\Blog\DeleteBlogRequest;
use App\Recipe;
use App\Http\Requests\Blog\CreateBlogRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\Blog\UpdateBlogRequest;
use App\ReadLater;
use App\Helpers\Constant;
use App\Helpers\FileHandler;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class BlogController extends Controller {

    /**
     * Default constructor
     */
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['index', 'show', 'featured']]);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");
        $page = (empty($request->input("page"))) ? 1 : $request->input("page");

        if ($searchTerm == null) {
            $blogs = Blog::with('user')->get();
            $recipes = Recipe::with('user')->get()->each(function($recipe, $key) use ($blogs) {
                $blogs->push($recipe);
            });
        } else {
            $blogs = Blog::where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere('content', 'LIKE', '%' . $searchTerm . '%')->with('user')->get();
            $recipes = Recipe::where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere('description', 'LIKE', '%' . $searchTerm . '%')->with('user')->get()->each(function($recipe, $key) use ($blogs) {
                $blogs->push($recipe);
            });
        }

        $blogs = $blogs->sortByDesc('created_at')->forPage($page, $pageCount)->sortByDesc('created_at');


        try {
            $user = JWTAuth::parseToken()->authenticate();
            $blogs->each(function ($curr, $key) use ($user) {
                $curr->load(['read_later' => function ($query) use ($user, $curr) {
                    $curr->is_readlater = $query->where('user_id', '=', $user->id)->count() > 0;
                }]);
            });
        } catch (\Exception $e) {
        }

        return $blogs->isEmpty()?abort(404):$blogs->values();
        //return $blogs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Blog\CreateBlogRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlogRequest $request) {
        $user = JWTAuth::parseToken()->authenticate($request->get('X-Auth-Token'));
        $blog = new Blog;
        $blog->fill($request->all());
        $blog->user()->associate($user);
        $blog->save();
        $oldTags = collect($request->tags)->pluck('id')->filter(function($value) {
            return $value;
        });
        $newTags = collect($request->tags)->filter(function($value, $key) {
            return !array_key_exists('id', $value);
        });
        if (!$oldTags->isEmpty()) {
            $blog->tags()->sync($oldTags->toArray());
        }
        if (!$newTags->isEmpty()) {
            $newTags = $newTags->map(function($tag) {
                return new Tag($tag);
            });
            $blog->tags()->saveMany($newTags);
        }
        $blog->save();
        return response()->json([
            'message' => 'Successfully added new blog',
            'entity' => $blog,
            'code' => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog) {
        $blog->load(['user', 'tags']);

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $blog->users_readlater = $blog->read_later->contains('user_id', $user->id);
        } catch(\Exception $e) {
        }
        return response()->json([
            'message' => 'Successfully retrieved an entity.',
            'entity' => $blog,
            'code' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Blog\UpdateBlogRequest $request
     * @param  Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlogRequest $request, Blog $blog) {
        $blog->fill($request->all());
        $oldTags = collect($request->tags)->pluck('id')->filter(function($value) {
            return $value;
        });
        $newTags = collect($request->tags)->filter(function($value, $key) {
            return !array_key_exists('id', $value);
        });
        if ($oldTags->isEmpty()) {
            $blog->tags()->detach();
        } else {
            $blog->tags()->sync($oldTags->toArray());
        }
        if (!$newTags->isEmpty()) {
            $newTags = $newTags->map(function($tag) {
                return new Tag($tag);
            });
            $blog->tags()->saveMany($newTags);
        }
        $blog->save();
        return response()->json([
            'message' => 'Successfully edited new blog',
            'entity' => $blog,
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteBlogRequest $request
     * @param  Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteBlogRequest $request, Blog $blog) {
        //$thumbnail = $blog->image_path;
        if (Blog::destroy($blog->id) > 0) {
            //$fileHandler = new FileHandler;
            //$fileHandler->removeFile($thumbnail);
            $message = 'Successfully deleted blog.';
            $code = 200;
        } else {
            $message = 'Internal server error';
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => null,
            'code' => $code
        ], $code);
    }

    /**
     * Latest added blogs
     * @return \Illuminate\Http\JsonResponse
     */
    public function latest() {
        $latest = Blog::orderBy('created_at', 'desc')->take(3)->get();
        return response()->json([
            'message' => 'Successfully retrieved an entity',
            'code' => 200,
            'entity' => $latest->values()
        ], 200);
    }

    /**
     * All blogs for admin
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function adminBlogs(Request $request) {
        $pageCount = (empty($request->input("per_page"))) ? 5 : $request->input("per_page");
        $searchTerm = (empty($request->input("search")) || $request->input("search") == "") ? null : $request->input("search");

        if ($searchTerm == null) {
            $blogs = Blog::with(['tags', 'user'])->orderBy('created_at', 'desc')->paginate($pageCount);
        } else {
            $blogs = Blog::with(['tags', 'user'])->where('title', 'LIKE', '%' . $searchTerm . '%')->orWhere('content', 'LIKE', '%' . $searchTerm . '%')->orderBy('created_at', 'desc')->paginate($pageCount);
        }
        return $blogs;
    }

    /**
     * Toggle featured for blog
     * @param Request $request
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleFeatured(Request $request, Blog $blog) {
        $featuredBlogs = Blog::where('featured', true)->get()->sortBy('featured_date');
        $featuredRecipes = Recipe::where('featured', true)->get()->sortBy('featured_date')->each(function ($item, $key) use ($featuredBlogs) {
            $featuredBlogs->push($item);
        });
        $featuredBlogs = $featuredBlogs->sortBy('featured_date');
        $old = $featuredBlogs->first();
        $count = $featuredBlogs->count();
        if ($request->get('featured')) {
            if ($count == 2) {
                $old->featured = false;
                $old->featured_date = null;
                $old->save();
            } else if ($count > 2) {
                $toUnfeature = $featuredBlogs->sortBy('featured_date')->slice(2);
                if (!$toUnfeature->isEmpty()) {
                    $toUnfeature->each(function ($item, $key) {
                        $item->featured = false;
                        $item->featured_date = null;
                        $item->save();
                    });
                }
            }
            $blog->featured = true;
            $blog->featured_date = Carbon::now();

            $toRet = [
                'message' => 'Successfully added Blog as Featured.',
                'code' => 200,
                'entity' => null
            ];
        } else {
            $blog->featured = false;
            $blog->featured_date = null;
            $toRet = [
                'message' => 'Successfully removed Blog as Featured.',
                'code' => 200,
                'entity' => null
            ];
        }
        $blog->save();
        return response()->json($toRet, 200);
    }

    /**
     * Toggle readLater for blog
     * @param Request $request
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleReadlater(Request $request, Blog $blog) {
        $user = JWTAuth::parseToken()->authenticate($request->get('X-Auth-Token'));

        $readLater = new ReadLater;
        $readLater->user()->associate($user);
        $readLater->entity()->associate($blog);
        $exists = ReadLater::where('user_id', '=', $user->id)->where('entity_type', '=', Constant::BLOG_IDENTIFIER)->where('entity_id', '=', $blog->id)->first();
        if ($exists) {
            $exists->delete();
        } else {
            $readLater->save();
        }
        return response()->json([
            'message' => 'Successfully updated read later list',
            'code' => 200,
            'entity' => $user->load('read_later')
        ], 200);
    }

    /**
     * Featured blogs and recipes
     * @return \Illuminate\Http\JsonResponse
     */
    public function featured() {
        $blogs = Blog::with('user')->where('featured', true)->get();
        $recipes = Recipe::with('user')->where('featured', true)->each(function ($item, $key) use ($blogs) {
            $blogs->push($item);
        });
        $blogs->sortBy('featured_date')->take(2);
        return response()->json([
            'message' => 'Successfully retrieved an entity',
            'code' => 200,
            'entity' => $blogs
        ], 200);
    }

    /**
     * Blog file upload
     * @param ImageRequest $request
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function blogFileUpload(ImageRequest $request, Blog $blog) {
        $basePath = Constant::absolutePath("BLOG_IMAGES_PATH");
        $file = $request->file('file');
        $fileHandler = new FileHandler;
        if ($file) {
            $blog->image_path = Constant::BLOG_IMAGES_PATH . $fileHandler->uploadPath($basePath)->addFile($file)->removeFile($blog->image_path)->first();
        }
        $blog->save();
        return response()->json([
            'message' => 'Successfully uploaded a file',
            'code' => 200,
            'entity' => $blog
        ], 200);
    }
}

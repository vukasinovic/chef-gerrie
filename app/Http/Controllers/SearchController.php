<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Recipe;
use App\Tag;
use Illuminate\Http\Request;
use TomLingham\Searchy\Facades\Searchy;

class SearchController extends Controller {
    /**
     * @param Request $request
     * @param null $keyword
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($keyword = null) {
        if ($keyword) {
            $tags = Tag::hydrate(Searchy::tags(['title'])->query($keyword)->get()->all());
            if($tags->count()) {
                $blog_results = collect();
                $recipe_results = collect();
                foreach($tags as $tag) {
                    $blog_results = $blog_results->merge($tag->blogs);
                }
                $blog_results->keyBy('id');
            } else {
                $blog_results = Blog::hydrate(Searchy::blogs(['title', 'content'])->query($keyword)->get()->all())->each(function ($blog, $key) {
                    $blog->load('tags', 'user');
                })->sortByDesc('created_at');

                $recipe_results = Recipe::hydrate(Searchy::recipes('title', 'description')->query($keyword)->get()->all())->each(function ($recipe, $key) {
                    $recipe->load('steps', 'user');
                })->sortByDesc('created_at');
            }
        } else {
            $blog_results = collect(Blog::with(['tags', 'user'])->get())->sortByDesc('created_at');
            $recipe_results = collect(Recipe::with(['steps', 'user'])->get())->sortByDesc('created_at');
        }

        return response()->json([
            $blog_results->values(),
            $recipe_results->values(),
        ]);
    }
}

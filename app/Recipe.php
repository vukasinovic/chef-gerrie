<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model {
    use SoftDeletes;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'ingredients', 'equipment', 'featured', 'featured_date'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'featured_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function steps() {
        return $this->hasMany('App\Step');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function read_later() {
        return $this->morphMany('App\ReadLater', 'entity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Setter for featured date
     *
     * @param $value
     */
    public function setFeaturedDate($value) {
        $this->attributes['featured_date'] = Carbon::now();
    }
}
